#define  MAX_SUFFIX     256

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004
#define  NOTFOUND_ERR   0xF0000008
#define  BADRECSIZE_ERR 0xF0000010

#define  GEO_TO_LONG    11930464.70556           // 217483647/180

char  acRawFile[_MAX_PATH];
char  acAdrFile[_MAX_PATH], acAdrTmpl[_MAX_PATH];
char  acDocPath[_MAX_PATH];
char  acLogFile[_MAX_PATH];
char  acIniFile[_MAX_PATH];
char  acOutFile[_MAX_PATH];
char  acCntyTbl[_MAX_PATH];
char  asSuffixTbl[MAX_SUFFIX][8];
char  bRegion, bExtType;
char  bMType;                       // Match type: 
                                    // S,A,Q=USPS data
                                    // D=mail box/small town general delivery
                                    // T=match street segment, zipcode is not provided
                                    // U=USPS data but no Zip4
                                    // X,Y=match intersection of two streets
                                    // Z=no address given, but zipcode is valid
int   iGeoLen, iApnLen, iRecLen;
int   iSkip, iFixCity, iLdrYear;
int   iChgStrNum, iChgStr, iChgSfx, iChgCity, iChgZip, iChgZip4, iUpdMail;
int   iMatch, iNoMatch, iMisMatch, iUseSameBP, iWrongFips, iBadAddr;
int   iMCode[2];

bool  bEnCode, bDebug, bIgnoreSitus, bOverwriteSitus, bLien, bSitusOnly, bIgnoreCity, bIgnoreZip, bReformatApn;
bool  bCreateMAdr, bReformatMapLink, bUseGeoAdr1, bUseGeoCity, bClrCity, bChkCity, bUseSfxXlat, 
      bUseGeoUnit, bUseGeoSNum, bUseGeoSStr;
bool  bUSPSOnly, bUseS01, bFormatDocLink, bSendMail, bWarning, bRemovePQZoning, bUseZip;

COUNTY_INFO myCounty;
extern CITY_INFO  myCityList[MAX_CITIES];
extern int        iNumCities, iNumSuffixes;