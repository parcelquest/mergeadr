#ifndef GEO_REC_MAX
#define  GEO_REC_MAX          512

// Field size
#define  GEO_SIZ_APN          14
#define  GEO_SIZ_STRNUM       7
#define  GEO_SIZ_FRA          3
#define  GEO_SIZ_DIR          2
#define  GEO_SIZ_STREET       24
#define  GEO_SIZ_SUFFIX       5
#define  GEO_SIZ_UNIT         6
#define  GEO_SIZ_CITY         24
#define  GEO_SIZ_STATE        2
#define  GEO_SIZ_ZIP          5
#define  GEO_SIZ_ZIP4         4
#define  GEO_SIZ_HOE          1
#define  GEO_SIZ_ADDR1        52
#define  GEO_SIZ_ADDR2        38
#define  GEO_SIZ_N_CARRT      4
#define  GEO_SIZ_N_LON        11
#define  GEO_SIZ_N_LAT        10
#define  GEO_SIZ_N_TRACT      14
#define  GEO_SIZ_N_MCODE      4
#define  GEO_SIZ_N_DPBC       2
#define  GEO_SIZ_N_CHECK      1
#define  GEO_SIZ_N_ADDRESS    60
#define  GEO_SIZ_N_LASTLINE   40
#define  GEO_SIZ_N_HOUSENUM   11
#define  GEO_SIZ_N_HOUSEFRA   3
#define  GEO_SIZ_N_HSENO      14
#define  GEO_SIZ_N_DIR        2
#define  GEO_SIZ_N_STREET     40
#define  GEO_SIZ_N_STRSUF     4
#define  GEO_SIZ_N_UNIT       11
#define  GEO_SIZ_N_CITY       28

#define  GEO_SIZ_CENSUS_BLK   9
#define  GEO_SIZ_MAPLINK      20

// Geo field offset
#define  GEO_OFF_APN          0
#define  GEO_OFF_STRNUM       14
#define  GEO_OFF_FRA          21
#define  GEO_OFF_DIR          24
#define  GEO_OFF_STREET       26
#define  GEO_OFF_SUFFIX       50
#define  GEO_OFF_UNIT         55
#define  GEO_OFF_CITY         61
#define  GEO_OFF_STATE        85
#define  GEO_OFF_ZIP          87
#define  GEO_OFF_ZIP4         92
#define  GEO_OFF_HOE          96
#define  GEO_OFF_ADDR1        97
#define  GEO_OFF_ADDR2        149
#define  GEO_OFF_N_CARRT      187
#define  GEO_OFF_N_LON        191
#define  GEO_OFF_N_LAT        202
#define  GEO_OFF_N_TRACT      212
#define  GEO_OFF_N_MCODE      226
#define  GEO_OFF_N_DPBC       230

// New additional fields
#define  GEO_OFF_N_CHECK      232
#define  GEO_OFF_N_ADDRESS    233
#define  GEO_OFF_N_LASTLINE   293
#define  GEO_OFF_N_HOUSENUM   333
#define  GEO_OFF_N_PREDIR     344 //340
#define  GEO_OFF_N_STREET     346 //342
#define  GEO_OFF_N_STRSUF     386 //366
#define  GEO_OFF_N_POSTDIR    390 //370
#define  GEO_OFF_N_UNIT       392 //372
#define  GEO_OFF_N_CITY       403 //378
#define  GEO_OFF_N_STATE      431 //402
#define  GEO_OFF_N_ZIP        433 //404
#define  GEO_OFF_N_ZIP4       438 //409

#define  GEO_OFF_GEO_FLG      442 //413
#define  GEO_OFF_CHG_FIPS     443 //414
#define  GEO_OFF_N_HOUSEFRA   444 //415
#define  GEO_OFF_N_HSENO      447 //418

// CD-Assessor field offset
#define  CDA_OFF_STRNUM       238
#define  CDA_OFF_DIR          248
#define  CDA_OFF_STREET       250
#define  CDA_OFF_SUFFIX       274
#define  CDA_OFF_CITY         285
#define  CDA_OFF_STATE        309
#define  CDA_OFF_ZIP          311
#define  CDA_OFF_ZIP4         316
#define  CDA_OFF_ADDR1        1652
#define  CDA_OFF_ADDR2        1704
#define  CDA_OFF_MAPLINK      1632

#define  CDA_OFF_FMTAPN       15
#define  CDA_SIZ_FMTAPN       17

#define  ADD_SIZE             19+GEO_SIZ_CENSUS_BLK+GEO_SIZ_N_CARRT+GEO_SIZ_N_DPBC

typedef struct _tGeoRec
{
   char  acCR[GEO_SIZ_N_CARRT];
   char  acLong[GEO_SIZ_N_LON];
   char  acLat[GEO_SIZ_N_LAT];
   char  acTract[GEO_SIZ_N_TRACT];
   char  acMCode[GEO_SIZ_N_MCODE];
   char  acDBPC[GEO_SIZ_N_DPBC];
   char  acCheck[GEO_SIZ_N_CHECK];
   char  acAddrLine[GEO_SIZ_N_ADDRESS];
   char  acLastLine[GEO_SIZ_N_LASTLINE];
   char  acHseNum[GEO_SIZ_N_HOUSENUM];
   char  acPreDir[GEO_SIZ_N_DIR];
   char  acStreet[GEO_SIZ_N_STREET];
   char  acSuffix[GEO_SIZ_N_STRSUF];
   char  acPostDir[GEO_SIZ_N_DIR];
   char  acUnit[GEO_SIZ_N_UNIT];
   char  acCity[GEO_SIZ_N_CITY];
   char  acState[GEO_SIZ_STATE];
   char  acZip[GEO_SIZ_ZIP+GEO_SIZ_ZIP4];
   char  bGeoCoded;                          // Y or blank
   char  bFipsChanged;                       // Y or blank
   char  acHseFra[GEO_SIZ_N_HOUSEFRA];
   char  acHseNo[GEO_SIZ_N_HSENO];
   char  filler[80];
} GEORECS;
#define  GEORECS_SIZE   sizeof(GEORECS)

typedef struct _tAdrExtr
{
   char  acApn[GEO_SIZ_APN];
   char  acStrNum[GEO_SIZ_STRNUM];
   char  acStrSub[GEO_SIZ_FRA];
   char  acStrDir[GEO_SIZ_DIR];
   char  acStrName[GEO_SIZ_STREET];
   char  acStrSfx[GEO_SIZ_SUFFIX];
   char  acUnit[GEO_SIZ_UNIT];
   char  acCity[GEO_SIZ_CITY];
   char  acState[GEO_SIZ_STATE];
   char  acZip[GEO_SIZ_ZIP+GEO_SIZ_ZIP4];
   char  HOE_Flag;
   char  acAddr1[GEO_SIZ_ADDR1];
   char  acAddr2[GEO_SIZ_ADDR2];
} ADREXTR;
#define  ADREXTR_SIZE   sizeof(ADREXTR)

typedef struct _tGeo512
{
   ADREXTR origAdr;
   char  acCR[GEO_SIZ_N_CARRT];
   char  acLong[GEO_SIZ_N_LON];
   char  acLat[GEO_SIZ_N_LAT];
   char  acTract[GEO_SIZ_N_TRACT];
   char  acMCode[GEO_SIZ_N_MCODE];
   char  acDBPC[GEO_SIZ_N_DPBC];
   char  acCheck[GEO_SIZ_N_CHECK];
   char  acAddrLine[GEO_SIZ_N_ADDRESS];
   char  acLastLine[GEO_SIZ_N_LASTLINE];
   char  acHseNum[GEO_SIZ_N_HOUSENUM];       // from 7 -> 11
   char  acPreDir[GEO_SIZ_N_DIR];
   char  acStreet[GEO_SIZ_N_STREET];         // from 24 -> 40
   char  acSuffix[GEO_SIZ_N_STRSUF];
   char  acPostDir[GEO_SIZ_N_DIR];
   char  acUnit[GEO_SIZ_N_UNIT];             // from 6 -> 11
   char  acCity[GEO_SIZ_N_CITY];             // from 24 -> 28
   char  acState[GEO_SIZ_STATE];
   char  acZip[GEO_SIZ_ZIP+GEO_SIZ_ZIP4];
   char  bGeoCoded;                          // Y or blank
   char  bFipsChanged;                       // Y or blank
   char  acHseFra[GEO_SIZ_N_HOUSEFRA];
   char  acHseNo[GEO_SIZ_N_HSENO];
   char  filler[49];
   char  CrLf[2];
} GEO512;
#define  GEO512_SIZE   sizeof(GEO512)

#endif
