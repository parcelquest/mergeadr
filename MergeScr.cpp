#include "stdafx.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "AttrRec.h"
#include "Logs.h"



extern   char acIniFile[];
extern   int  iApnLen;

LU_ENTRY tblAttr[24];

char  *myTrim(char *pString);
void  replChar(char *pBuf, char cSrch, char cRepl);
void  blankPad(char *pBuf, int iLen);
int   atoin(char *pStr, int iLen);

/************************************ LoadLUTable *****************************
 *
 * 
 *
 *****************************************************************************/

int LoadLUTable(char *pLUFile, char *pTableName, LU_ENTRY *pTable, int iMaxEntries)
{
   char  acTmp[_MAX_PATH], *pTmp;
   int   iTmp;
   FILE  *fd;

   fd = fopen(pLUFile, "r");
   if (fd)
   {
      //iTmp = 0;
      // Find table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && !stricmp(myTrim(acTmp), pTableName))
            break;
         //iTmp++;
      }

      iTmp = 0;
      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && *pTmp > ' ')
         {
            strcpy(pTable->acIndex, myTrim(acTmp));
            pTmp = strchr(pTable->acIndex, ' ');
            *pTmp = 0;                             // Mark end of index string
            iTmp++;
            pTable++;
            if (iTmp >= iMaxEntries)
            {
               pTable->acIndex[0] = 0;
               pTable->acValue[0] = 0;
               break;
            }
         } else
            break;
      }

      fclose(fd);
   } else
      LogMsg("Error opening lookup table %s", pLUFile);

   return iTmp;
}

/********************************** Value2Code *******************************
 *
 * Return -1 if not found, else index number
 *
 *****************************************************************************/

int Value2Code(char *pValue, char *pCode, LU_ENTRY *pTable)
{
   int iRet=-1, iTmp=0;
   
   *pCode = 0;
   while (pTable->acIndex[0] > ' ')
   {
      if (!strcmp(pValue, pTable->acValue))
      {
         strcpy(pCode, pTable->acIndex);
         iRet = iTmp;
         break;
      }
      iTmp++;
      pTable++;
   }
   return iRet;
}

/********************************** MergeScrAttr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int MergeAttrRec(char *pAttrRec, char *pOutbuf)
{
   SCR_ATTR *pRec;
   char     acTmp[256];
   long     lTmp; 
   double   dTmp;
   int      iRet, iTmp, iTmp1;

   pRec = (SCR_ATTR *)pAttrRec;

   pRec->filler1[0] = ' ';
   pRec->CrLf[0] = 0;

   // Bldg Class/Bldg Quality
   iRet = Value2Code(pRec->BldgQual, acTmp, &tblAttr[0]);
   blankPad(acTmp, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acTmp, SIZ_BLDG_QUAL);
   *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgClass;

   // Improve SF - OFF_BLDG_SF
   memcpy(acTmp, pRec->Imps, SIZ_ATTR_IMPS);
   acTmp[SIZ_ATTR_IMPS] = 0;
   lTmp = atol(acTmp);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d  ", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // LotSize, LotAcre
   memcpy(acTmp, pRec->LotAcre, SIZ_ATTR_LOTACRE);
   acTmp[SIZ_ATTR_LOTACRE] = 0;
   dTmp = atof(acTmp)*1000;
   if (dTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   memcpy(acTmp, pRec->LotSize, SIZ_ATTR_LOTSIZE);
   acTmp[SIZ_ATTR_LOTSIZE] = 0;
   dTmp = atof(acTmp) + 0.5;           // round up
   if (dTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_LOT_SQFT, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ATTR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = atoin(pRec->Beds, SIZ_ATTR_BEDS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths  OFF_BATH_F
   if (pRec->Bath > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Bath;
   }
   if (pRec->HBath > '0')
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = pRec->HBath;
   }

   // YrBlt, YrEff  OFF_YR_BLT
   if (pRec->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, SIZ_YR_BLT);
   if (pRec->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, SIZ_YR_EFF);
   
   // Roof Type
   *(pOutbuf+OFF_ROOF_TYPE) = pRec->RoofType;

   // Garage size, Park type
   iTmp = atoin(pRec->GarSize, SIZ_ATTR_GARSIZE);
   if (iTmp > 0)
   {
      iTmp1 = atoin(pRec->CarPortSF, SIZ_ATTR_CPSIZE);
      sprintf(acTmp, "%*d ", SIZ_GAR_SQFT, iTmp);
      if (iTmp1 > 0)
         *(pOutbuf+OFF_PARK_TYPE) = '2';     // GARAGE/CARPORT
      else
         *(pOutbuf+OFF_PARK_TYPE) = 'Z';     // GARAGE
   } else if (pRec->CarPort == 'N' && pRec->Garage != 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   else if (pRec->CarPort != 'N' && pRec->Garage == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'C';        // CARPORT
   else
   {
      iTmp = atoin(pRec->CarPortSF, SIZ_ATTR_CPSIZE);
      if (iTmp > 0)
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
   }

   // Pool, Spa
   if (pRec->Pool == 'Y' && pRec->Spa == 'Y')
      *(pOutbuf+OFF_POOL) = 'C';
   else if (pRec->Pool == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';
   else if (pRec->Spa == 'Y')
      *(pOutbuf+OFF_POOL) = 'S';

   // Const Type OFF_CONS_TYPE
   *(pOutbuf+OFF_CONS_TYPE) = pRec->Const;

   // Units
   iTmp = atoin(pRec->Units, SIZ_ATTR_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Fp, Heat type OFF_FIRE_PL
   if (pRec->FirePlace > '0')
   {
      *(pOutbuf+OFF_FIRE_PL) = ' ';
      *(pOutbuf+OFF_FIRE_PL+1) = pRec->FirePlace;
   }
   *(pOutbuf+OFF_HEAT) = pRec->Heat;

   // View  
   if (pRec->ViewFlag != 'N')
      *(pOutbuf+OFF_VIEW) = pRec->ViewFlag;

   return iRet;
}

/********************************* MergeScrAttr ******************************
 *
 * Merge Attr file to R01
 *
 *****************************************************************************/

int MergeScrAttr(char *pIniFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acAttrFile[_MAX_PATH], acLUFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fd;

   int      iRet, iTmp, iRecLen, iNoMatch;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Get raw file name
   GetPrivateProfileString("Data", "RawFile", "", acBuf, _MAX_PATH, pIniFile);
   iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, pIniFile);
  
   // Get Attr file name
   GetPrivateProfileString("SCR", "AttrFile", "", acAttrFile, _MAX_PATH, pIniFile);

   // Get Lookup file name
   GetPrivateProfileString("System", "LookUpTbl", "", acLUFile, _MAX_PATH, pIniFile);

   // Save original file
   sprintf(acOutFile, acBuf, "SCR", "SCR", "R01");
   sprintf(acRawFile, acBuf, "SCR", "SCR", "S01");
   iRet = 0;
   if (!_access(acOutFile, 0))
   {
      if (!_access(acRawFile, 0))
         iRet = remove(acRawFile);

      if (!iRet)
         iRet = rename(acOutFile, acRawFile);
      if (iRet)
      {
         LogMsg("*** Error renaming input file.  Please check .R01 and .S01 files");
         return 1;
      }
   } else
   {
      LogMsg("*** Missing input file %s.  Please check.", acOutFile);
      return 1;
   }

   //iApnLen = 8;

   // Load tables
   iRet = LoadLUTable((char *)&acLUFile[0], "[Quality]", &tblAttr[0], MAX_ATTR_ENTRIES);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Quality] in %s", acLUFile);
      return 1;
   }

   // Open Attr file
   LogMsg("Open Attr file %s", acAttrFile);
   fd = fopen(acAttrFile, "r");
   if (fd == NULL)
   {
      LogMsg("*** Error opening Attr file: %s\n", acAttrFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("*** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("*** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRec[0], SIZE_SCR_ATTR, fd);
   bEof = (pTmp ? false:true);

   // Copy skip record 
   memset(acBuf, ' ', iRecLen);
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = 0;
            fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
               break;
         } else
            break;
      }

      NextRec:
      //if (!memcmp(acRec, "08017419", 8))
      //   iTmp = 0;
      iTmp = memcmp(acBuf, acRec, iApnLen);
      if (!iTmp)
      {
         // Merge data
         try
         {
            iRet = MergeAttrRec(acRec, acBuf);
         } catch(...)
         {
            iTmp = 0;
         }

         // Read next record
         pTmp = fgets(acRec, SIZE_SCR_ATTR, fd);

         if (!pTmp)
            bEof = false;    // Signal to stop 
      } else
      {
         if (iTmp > 0)        // Attr not match, advance to next record
         {
            LogMsg("***** Error Attr not match : Attr->%.*s <> R01->%.*s (%d) *****", iApnLen, acRec, iApnLen, acBuf, lCnt);
            pTmp = fgets(acRec, SIZE_SCR_ATTR, fd);

            if (!pTmp)
               bEof = false;    // Signal to stop
            else
            {
               LogMsg("***** Next record: %.*s *****", iApnLen, acRec);
               goto NextRec;
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      LogMsg("*** No match R01= %.*s", iApnLen, acBuf);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      iNoMatch++;
      lCnt++;
   }

   // Close files
   if (fd)
      fclose(fd);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unmatched records:    %u", iNoMatch);

   printf("\nTotal output records: %u", lCnt);
   return 0;
}

