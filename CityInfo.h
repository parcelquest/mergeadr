
typedef struct T_CITY_INFO
{
   char  acCityName[24];
   char  acState[4];
   int   iOldCityID;
   int   iNewCityID;
} CITY_INFO;