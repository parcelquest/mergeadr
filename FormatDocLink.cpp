/******************************************************************************
 *
 * These functions are extracted from LoadCres 7/30/2014
 *
 ******************************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "Prodlib.h"
#include "getopt.h"
#include "Logs.h"
#include "Georec.h"
#include "Geostats.h"
#include "CountyInfo.h"

#define  _DOC_LINK      1
#include "R01.h"

#include "FormatApn.h"
#include "RecDef.h"
#include "Utils.h"
#include "Tables.h"
#include "FormatDocLink.h"

extern char acDocPath[];

/******************************* Tri_MakeDocLink *******************************
 *
 * Format DocLink 
 *
 ******************************************************************************/

void Tri_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   int   iTmp, iDocNum;
   char  acTmp[256], acDocName[256];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      iTmp = atoin((char *)pDate, 4);
      iDocNum = atoin((char *)pDoc+4, 5);

      if (iTmp >= 1990 && iDocNum > 0)
      {
         sprintf(acDocName, "%.4s\\%.3s\\%.4s%.5d", pDoc, pDoc+4, pDoc, iDocNum);
         sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);
         if (!_access(acTmp, 0))
            strcpy(pDocLink, acDocName);
      }
   }
}


/****************************** Tri_FmtDocLinks *****************************
 *
 * Return > 0 if there is doclink assigned.
 *
 ****************************************************************************/

int Tri_FmtDocLinks(char *pR01, char *pCnty, int iType)
{
   char acDocLink[64], acDocLinks[256];
   int  iTmp;

   // Reset old links
   memset(pR01+OFF_DOCLINKS, ' ', SIZ_DOCLINKS);
   acDocLinks[0] = 0;

   // Doc #1
   Tri_MakeDocLink(acDocLink, pR01+OFF_SALE1_DOC, pR01+OFF_SALE1_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);
   strcat(acDocLinks, ",");

   // Doc #2
   Tri_MakeDocLink(acDocLink, pR01+OFF_SALE2_DOC, pR01+OFF_SALE2_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);
   strcat(acDocLinks, ",");

   // Doc #3
   Tri_MakeDocLink(acDocLink, pR01+OFF_SALE3_DOC, pR01+OFF_SALE3_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);
   strcat(acDocLinks, ",");

   // Transfer Doc
   Tri_MakeDocLink(acDocLink, pR01+OFF_TRANSFER_DOC, pR01+OFF_TRANSFER_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);

   // Update DocLinks
   iTmp = strlen(acDocLinks);
   if (iTmp > 10)
      memcpy(pR01+OFF_DOCLINKS, acDocLinks, iTmp);

   return iTmp;
}

/******************************* Tuo_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

/******************************* Tuo_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Tuo_MakeDocLink(char *pDocLink, char *pDoc, char *pDate)
{
   int   iTmp, iDocNum;
   char  acTmp[256], acDocName[256];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      iTmp = atoin((char *)pDate, 4);
      iDocNum = atoin((char *)pDoc, 8);

      if (iTmp >= 2002 && iDocNum > 0)
         sprintf(acDocName, "%.4s\\%.3s\\%.4s%06d", pDate, pDoc, pDate, iDocNum);
      else
         sprintf(acDocName, "%.4s\\%.4s\\%.4s%.8s", pDate, pDoc, pDate, pDoc);

      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
         else
            *pDocLink = 0;
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

/****************************** Tuo_FmtDocLinks *****************************
 *
 * Format DocLinks as bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp
 * which are Doclink1, Doclink2, Doclink3, DocXferlink
 *
 * Return > 0 if there is doclink assigned.
 *
 ****************************************************************************/

int Tuo_FmtDocLinks(char *pR01, char *pCnty, int iType)
{
   char acDocLink[64], acDocLinks[256];
   int  iTmp;

   // Reset old links
   memset(pR01+OFF_DOCLINKS, ' ', SIZ_DOCLINKS);
   acDocLinks[0] = 0;

   // Doc #1
   Tuo_MakeDocLink(acDocLink, pR01+OFF_SALE1_DOC, pR01+OFF_SALE1_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);
   strcat(acDocLinks, ",");

   // Doc #2
   Tuo_MakeDocLink(acDocLink, pR01+OFF_SALE2_DOC, pR01+OFF_SALE2_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);
   strcat(acDocLinks, ",");

   // Doc #3
   Tuo_MakeDocLink(acDocLink, pR01+OFF_SALE3_DOC, pR01+OFF_SALE3_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);
   strcat(acDocLinks, ",");

   // Transfer Doc
   Tuo_MakeDocLink(acDocLink, pR01+OFF_TRANSFER_DOC, pR01+OFF_TRANSFER_DT);
   if (*acDocLink)
      strcat(acDocLinks, acDocLink);

   // Update DocLinks
   iTmp = strlen(acDocLinks);
   if (iTmp > 10)
      memcpy(pR01+OFF_DOCLINKS, acDocLinks, iTmp);

   return iTmp;
}

