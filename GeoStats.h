#ifndef GeoStats

#define  CHG_SUFFIX     1
#define  CHG_PREDIR     2
#define  CHG_POSDIR     4
#define  CHG_STREET     8
#define  CHG_ZIPCODE    1
#define  CHG_CITY       2
#define  CHG_STATE      4
#define  CHG_ZIP4       8

/* a GeoStats struct holds the goecoder report needed stats */
typedef struct GeoStats
{
   long records;                       /* total records processed */
   long matched;                       /* total records matched */
   long processed;                     /* total records processed(uMatchOnly) */
   long Std;                           /* Standardized matched records */
   long xSect;                         /* Intersection matched records */
   long nonusps;                       /* non USPS type matches */
   long E01;                           /* E001 non matched records */
   long E02;                           /* E002 non matched records */
   long E03;                           /* E003 non matched records */
   long E04;                           /* E004 non matched records */
   long E10;                           /* E010 non matched records */
   long E11;                           /* E011 non matched records */
   long E12;                           /* E012 non matched records */
   long E13;                           /* E013 non matched records */
   long E14;                           /* E014 non matched records */
   long E15;                           /* E015 non matched records */
   long E20;                           /* E020 non matched records */
   long E21;                           /* E021 non matched records */
   long E22;                           /* E022 non matched records */
   long E23;                           /* E023 non matched records */
   long E24;                           /* E024 non matched records */
   long E25;                           /* E025 non matched records */
   long E26;                           /* E026 non matched records */
   long E27;                           /* E027 non matched records */
   long E28;                           /* E028 non matched records */
   long corAddLine;                    /* Total corrected address line, any item */
   long corLastLine;                   /* Total corrected Lastline, any item */
   long corCtyName;                    /* Matches with corrected city name */
   long corStaAbb;                     /* Matches with corrected state abbreviation */
   long corZIPCode;                    /* Matches with corrected ZIP code */
   long corZIP4Code;                   /* Matches with corrected ZIP +4 add-on */
   long corPreDir;                     /* Matches with corrected pre-directional */
   long corStrName;                    /* Matches with corrected street name */
   long corStrType;                    /* Matches with corrected street type*/
   long corPostDir;                    /* Matches with corrected post-directional */
   long geoed;                         /* matched records with Geocodes assigned */
   long geoaddr;                       /* matched, Geocoded with address location */
   long centZIP4;                      /* Centroid match w/ ZIP+4 accuracy */
   long centZIP2;                      /* Centroid match w/ ZIP+2 accuracy */
   long centZIP;                       /* Centroid match w/ ZIP code accuracy */
   long centBlk;                       /* Centroid match w/ Block group accuracy */
   long centCens;                      /* Centroid match w/ Census Tract accuracy */
   long centCnty;                      /* Centroid match w/ County accuracy */
   time_t startDate;                   /* Processing Start Date and Time */
   time_t endDate;                     /* Processing Start Date and Time */
   long z4ChangeSkipped;               /* Records skipped because Z4Change said they didn't need to be updated */
} GeoStats;

static GeoStats report =
{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
#endif