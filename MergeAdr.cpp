/********************************** MergeAdr ********************************
 *
 * Merge address, GrGr, and Attribute data to roll file
 *
 * 08/29/2008 1.9    Remove check that only populates addr1 if original is blank.  
 *                   This idea won't work since we want to use Centrus to correct 
 *                   our addresses.  This will fix DNX problem where county 
 *                   provides non-standard addresses -spn
 * 09/01/2008 1.9.1  Add bUseGeoAdr1 (default true) and bUseGeoCity (default false)
 *                   to allow user to skip certain county from using geocoded address.
 *                   Flag is set in INI file.
 * 09/23/2008 2.0    If Addr1 is modified by Centrus, update only if GAdr1 is true.
 *                   Change INI file: City -->IgnoreCity to ignore geocoded city name
 *                   Add ClrCity to clear old city if new one not geocoded.           
 *                   Add ChkCity to verify that old city is the same as geocoded before update zipcode.
 *                   Add IgnoreZip to ignore geocoded zipcode (NEV)
 * 12/09/2008 2.1    Fix to support NEV. Drop options to merge GrGr, Lien, and Attr.  
 *                   Remove unused code.
 * 12/10/2008 2.1.1  Make StrNum left justify.
 * 03/09/2009 2.1.3  When update situs addr1, blank out StrSub to make it cleaned.
 * 05/05/2009 2.1.4  Add -Fa option to reformat APN of GIS file.
 * 05/22/2009 2.1.6  Adding option to reformat maplink on INI file.  Adding FmtMapLink=Y
 *                   in county section to force reformat MapLink.
 * 01/06/2010 2.2.0  Add bUSPSOnly and option to use USPS address only.
 * 01/09/2009 2.2.2  Add bUseS01 and option to merge situs to S01 instead R01 file
 * 02/01/2010 2.3.0  Merge fraction when avaiable.  This version support both 416 & 512
 *                   GEORECS layouts.  Geosize can be specified in INI file under county section.
 * 02/01/2010 2.3.1  If the first 8 bytes are the same, use Centrus address (RIV 669522015).
 *                   Open Geo file in binary to make sure it align correctly.  This fix 512  problem.
 * 06/28/2010 2.4.0  Add flag to identify parcels that use GIS lat/long.
 * 10/19/2010 2.5.0  Add iLdrYear to support merge LDR file from prior year use in ITRaC.
 * 04/21/2011 2.6.0  Modify MergeSitus() to replace StrName only if it contains more than two character.
 * 06/21/2011 2.7.0  Change default of bUseGeoAdr1 to true so it won't update existing
 *                   addr1 unless specified in INI file such as DNX and RIV.
 * 06/23/2011 2.7.1  Fix bug in LoadCountyInfo() that crashed when running ORG.
 * 06/28/2011 2.8.0  Add -Fd option to reformat DOCLINK.
 * 10/25/2011 2.9.0  Modify MergeSitus() to populate situs if roll file doesn't provide (ORG).
 * 08/07/2012 2.11   Stop processing if blank record found.
 * 08/20/2012 2.11.2 Modify MakeNewFields() to check on valid LON before update to fix problem
 *                   when LAT is right justfied.
 * 05/01/2013 2.12.0 Fix MergeSitus(), don't update adr if strNum is not avail.
 * 01/03/2014 2.13.0 Copy StrNum to HseNo if HseNo is blank.
 *            2.13.2 Modify copyStrNum() to reset HSENO before copy.
 * 03/14/2014 2.13.3 Fix zipcode update.
 * 07/29/2014 14.0.0 Add Tri_FmtDocLinks().
 * 08/06/2014 14.1.0 Add -E email option. Check GIS file and report error on empty file.
 * 08/14/2014 14.1.1 Add config option to ignore warning email.
 * 08/21/2014 14.1.2 Only guess city & zip when there is no City name in raw record.
 * 02/04/2015 14.2.0 Replace GetPrivateProfileString() with GetIniString() which will 
 *                   translate [Machine] to local machine name and [CO_DATA] with CO_DATA 
 *                   token defined in INI file. 
 * 02/25/2015 14.3.0 Reverse the default of bUseGeoCity to 'N'.  If county set to 'Y', 
 *                   the geocoded city name will be used. Add option GCity to INI file.
 * 04/20/2015 14.3.1 Log street name change
 * 06/23/2015 15.0.0 Do not auto format MapLink when it's blank
 * 08/31/2015 15.1.1 Modify MergeSitus() to fix street name being chopped off.
 * 04/19/2016 15.1.2 Change log msg.
 * 11/17/2016 16.2.0 Modify MergeSitus() to update StrNum only if it's blank and only use first number.
 * 11/17/2017 17.0.0 Modify MergeSitus() to blank out StrSub only if we are about to change (Fix SCL)
 * 01/03/2018 17.1.0 Modify MakeGuessFields() to blank out Long/Lat.
 * 06/26/2018 17.2.0 Modify MergeSitus() ignore StrNum change, keep the county version.
 *                   Also not to update HSENO.  Add format DocLink option in INI file for county specific.
 * 06/30/2018 18.0.0 Bug fix in MergeSitus()
 * 12/18/2018 18.0.1 Modify Usage() to display more precise option message (-Y).
 * 02/15/2019 18.1.0 Add -W option to overwrite situs with geocoded file.  
 *                   You can also add it to INI file by setting Overwrite=Y
 * 04/08/2019 18.2.0 Add Unit# to fix bug that caused by code change in 20150831.  This only
 *                   affects street name longer than 12 bytes and has trailing unit#.
 * 04/20/2019 18.2.1 Update S_UNIT only if it's blank (Test on CCX)
 * 04/21/2019 18.2.2 Bug fix - remove extended geocoding portion when not matched
 * 04/22/2019 18.2.3 Bug fix - reset S_HSENO too when ignore situs is true
 * 04/24/2019 18.2.4 Bug fix - update S_HSENO whenever updating S_STRNUM
 * 07/23/2019 19.0.0 Add option to ignore StrNum & Unit# (fix SDX).
 * 08/15/2020 20.0.0 Add option to remove PQZoning per county (RemPQZoning=Y)
 * 05/12/2021 20.2.0 Don't guess city name, use what county provided only
 * 05/25/2021 20.3.0 Add option to update zip code [USPSZip] in INI file.  When set to 'Y'
 *                   zip code will be updated using USPS version.
 * 03/19/2022 21.0.0 Add INI option GSStr=N to skip update geo situs street name.
 * 03/21/2022 21.1.0 Modify MergeSitus() not to update street suffix and unit if bUseGeoSStr is false.
 * 05/17/2022 21.2.0 When GCITY=N, do not update city.  Keep whatever just updated from county file.
 * 06/21/2022 22.0.0 Clear S_FRA if matching Centrus's S_UNIT (BUT).
 * 08/17/2022 22.1.0 Modify -U to specify input extension.
 * 01/05/2023 22.2.0 Use post DIR if available (RIV).  Copy it to S_DIR instead.
 * 11/02/2023 23.0.0 Update Usage() to add -Uv
 *
 ****************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "getopt.h"
#include "Logs.h"
#include "Georec.h"
#include "Geostats.h"
#include "CountyInfo.h"
#include "R01.h"
#include "FormatApn.h"
#include "RecDef.h"
#include "Utils.h"
#include "Tables.h"
#include "MergeAdr.h"
#include "resource.h"
#include "FormatDocLink.h"
#include "SendMail.h"

CWinApp theApp;
using namespace std;

void  RemapCity(char *pRec);
void  GuessCityZip(char *pGeoRec, char *pRec);

/******************************** formatMAdr() *******************************
 *
 * Format M_ADDR_D and place it in buffer
 *
 *****************************************************************************/

void formatMAdr(char *pOutbuf)
{
   char  acTmp[256], acCity[32], *pTmp;
   int   iTmp;

   memcpy(acTmp, pOutbuf+OFF_M_STRNUM, SIZ_M_ADDR1);
   blankRem(acTmp, SIZ_M_ADDR1);
   memcpy(pOutbuf+OFF_M_ADDR_D, acTmp, strlen(acTmp));

   memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
   myTrim(acCity, SIZ_M_CITY);
   if (acCity[0] > ' ')
   {
      if (pTmp = strchr(acCity, ','))
         iTmp = sprintf(acTmp, "%s %.5s", acCity, pOutbuf+OFF_M_ZIP);
      else
         iTmp = sprintf(acTmp, "%s, %.2s %.5s", acCity, pOutbuf+OFF_M_ST, pOutbuf+OFF_M_ZIP);

      if (iTmp > SIZ_M_CTY_ST_D) iTmp = SIZ_M_CTY_ST_D;
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }
}

/******************************** chkMatchCode() *****************************
 *
 * Parsing match code
 *
 *****************************************************************************/

int chkMatchCode(char mcode[])
{
   int   iRet=0;

   // track Standardized matched records 
   if(mcode[0] == 'S' || mcode[0] == 'A' || mcode[0] == 'U' || mcode[0] == 'D' || mcode[0] == 'Q')
   { report.Std++; }

   // track Intersection matched records 
   if(mcode[0] == 'X' || mcode[0] == 'Y')
   { report.xSect++; }

   // track Non-USPS matched records 
   if(mcode[0] == 'T')
   {  report.nonusps++; }

   if((mcode[1] & 1) || (mcode[1] & 2) || (mcode[1] & 4) || (mcode[1] & 8))
   { report.corLastLine++; }

   if((mcode[2] & 1) || (mcode[2] & 2) || (mcode[2] & 4) || (mcode[2] & 8))
   { report.corAddLine++; }

   if(mcode[1] & 1)
   { report.corZIPCode++;}
   if(mcode[1] & 2)
   { report.corCtyName++;}
   if(mcode[1] & 4)
   { report.corStaAbb++;}
   if(mcode[1] & 8)
   { report.corZIP4Code++;}

   if(mcode[2] & 1)
   { report.corStrType++;}
   if(mcode[2] & 2)
   { report.corPreDir++;}
   if(mcode[2] & 4)
   { report.corPostDir++;}
   if(mcode[2] & 8)
   { report.corStrName++;}
   return iRet;
}

/*********************************** RemapCity() *****************************
 *
 * Remap old cityID to new one.  If IgnoreCity or IgnoreSitus flags set, clear
 * the situs city field.
 *
 *****************************************************************************/

void RemapCity(char *pRec)
{
   int   iIdx;
   int   iOldCityID;
   char  acTmp[_MAX_PATH];

   iOldCityID = atoin(pRec+OFF_S_CITY, 3);
   if (!bIgnoreCity && !bIgnoreSitus && iOldCityID > 0)
   {
      for (iIdx=1; iIdx <= iNumCities; iIdx++)
      {
         if (iOldCityID == myCityList[iIdx].iOldCityID)
         {
            if (iOldCityID != myCityList[iIdx].iCityID)
            {
               sprintf(acTmp, "%d                           ", myCityList[iIdx].iCityID);
               memcpy(pRec+OFF_S_CITY, acTmp, GEO_SIZ_CITY);
            }
            break;
         }
      }

      if (iIdx > iNumCities)
      {
         // Not found
         LogMsg0("Invalid old cityID %d", iOldCityID);
         memset(pRec+OFF_S_CITY, ' ', GEO_SIZ_CITY);
      } else if (iOldCityID != myCityList[iIdx].iCityID)
      {
         if (bDebug)
            LogMsg0("Remap city for APN=%.*s from %d to %d", myCounty.iApnLen, pRec,
            iOldCityID, myCityList[iIdx].iCityID);
      }
   } else
      memset(pRec+OFF_S_CITY, ' ', GEO_SIZ_CITY);
}

/********************************* ValidateSfx *******************************
 *
 *
 *****************************************************************************/

bool ValidateSfx(char *pSuffix)
{
   int   iTmp;
   bool  bRet;
 
   bRet = false;

   if (!*pSuffix || *pSuffix == ' ')
      return bRet;

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      if (!strcmp(pSuffix, asSuffixTbl[iTmp]))
      {
         bRet = true;
         break;
      }
   }

   return bRet;
}

/************************************ GeoToLong ******************************
 *
 *
 *****************************************************************************/

void GeoToLong(char *pLong, char *pLat, char *pOutbuf)
{
   unsigned long lLong, lLat;
   double   dLong, dLat;

   if (pLong && *pLong && pLat && *pLat)
   {
      dLong = atof(pLong);
      dLat = atof(pLat);
      lLat = (unsigned long)(dLat * GEO_TO_LONG);
      lLong = (unsigned long)(dLong * GEO_TO_LONG);
      sprintf(pOutbuf, "%.10u%.9u", lLong, lLat);
   } else
   {
      strcpy(pOutbuf, "                   ");     // 19 spaces
   }

}

/************************************ GeoToLong ******************************
 *
 *
 *****************************************************************************/

void GeoToLong(char *pInbuf, char *pOutbuf)
{
   char  *pFlds[4], acInbuf[256];

   strcpy(acInbuf, pInbuf);
   ParseString(acInbuf, ',', 3, pFlds);
   GeoToLong(pFlds[1], pFlds[2], pOutbuf);
}

int Hex2Bin(char cChar)
{
   int iRet=0;

   switch (cChar)
   {
      case '1':
         iRet = 1;
         break;
      case '2':
         iRet = 2;
         break;
      case '3':
         iRet = 3;
         break;
      case '4':
         iRet = 4;
         break;
      case '5':
         iRet = 5;
         break;
      case '6':
         iRet = 6;
         break;
      case '7':
         iRet = 7;
         break;
      case '8':
         iRet = 8;
         break;
      case '9':
         iRet = 9;
         break;
      case 'A':
         iRet = 10;
         break;
      case 'B':
         iRet = 11;
         break;
      case 'C':
         iRet = 12;
         break;
      case 'D':
         iRet = 13;
         break;
      case 'E':
         iRet = 14;
         break;
      case 'F':
         iRet = 15;
         break;
   }

   return iRet;
}

/*********************************** ReadGeoRec ******************************
 *
 *
 *****************************************************************************/

char *ReadGeoRec(char *pRec, int iLen, FILE *fd)
{
   char *pRet;
   int   iRet;

   pRet = NULL;
   while (!pRet && !feof(fd))
   {
      if (iLen > 0)
      {
         iRet = fread(pRec, 1, iLen, fd);
         bMType = *(pRec+GEO_OFF_N_MCODE);

         iMCode[0] = Hex2Bin(*(pRec+GEO_OFF_N_MCODE+1));
         iMCode[1] = Hex2Bin(*(pRec+GEO_OFF_N_MCODE+2));

         if (iRet > 0)
         {
            // If bad address, get next one
            if (*(pRec+GEO_OFF_N_MCODE) == 'Z')
            {
               LogMsg0("Unmatched Code: %.3s : %.100s", pRec+GEO_OFF_N_MCODE, pRec);
               iBadAddr++;
            } else if (memcmp(myCounty.acFipsCode, pRec+GEO_OFF_N_TRACT, 5) && (iMCode[0] & CHG_ZIPCODE) )
            {
               LogMsg0("Zip & FIPS changed: %.5s : %.100s", pRec+GEO_OFF_N_TRACT, pRec);
               iBadAddr++;
            } else
            {
#ifdef _DEBUG
               //if (!memcmp(pRec, "14122302", 8) )
               //   iRet = 0;
#endif

               if (*(pRec+GEO_OFF_N_MCODE) == 'S' && (iMCode[1] & CHG_STREET))
               {
                  LogMsg0("*** 1. Street name changed [%.*s]: %.24s to %.40s", iApnLen, pRec, pRec+GEO_OFF_STREET, pRec+GEO_OFF_N_STREET);
               }
               pRet = pRec;
            }
         }
      } else
         pRet = fgets(pRec, GEO_REC_MAX-1, fd);
   }
   return pRet;
}

/********************************** City2Code *******************************
 *
 * We use the assumption that parcels in the same city are group together.  So 
 * using static index would help speeding up the matching process.
 *
 *****************************************************************************

void City2Code(char *pCityName, char *pCityCode)
{
   static int iIdx=1;
   int iTmp;

   // Continue with last city
   iTmp = iIdx;
   for (; iIdx <= iNumCities; iIdx++)
   {
      if (!strcmp(pCityName, myCityList[iIdx].acCityName))
      {
         sprintf(pCityCode, "%d                           ", myCityList[iIdx].iNewCityID);
         break;
      }
   }

   // Restart from top of the list if not found
   if (iIdx > iNumCities && iTmp > 1)
   {
      for (iIdx = 1; iIdx <= iNumCities; iIdx++)
      {
         if (!strcmp(pCityName, myCityList[iIdx].acCityName))
         {
            sprintf(pCityCode, "%d                           ", myCityList[iIdx].iNewCityID);
            break;
         }
      }
   }

   if (iIdx > iNumCities)
   {
      // Reset index
      iIdx = 1;

      // Not found
      LogMsg0("Invalid city name %s", pCityName);
      memset(pCityCode, ' ', GEO_SIZ_CITY);
      *(pCityCode+GEO_SIZ_CITY) = 0;
      iBadCity++;
   }
}

/********************************* GuessCityZip() ****************************
 *
 * This function will copy city and zipcode from nearby parcel if roll record 
 * is blank.
 *
 *****************************************************************************/

void GuessCityZip(char *pGeoRec, char *pRec)
{
   char acCity[GEO_SIZ_CITY*2];
   char acTmp[_MAX_PATH];

   if (bIgnoreSitus)
      return;

   if (bIgnoreCity)
   {
      memset(pRec+OFF_S_CITY, ' ', GEO_SIZ_CITY);
      return;
   }

   // If Roll city is blank, use GeoRec
   if (*(pRec+OFF_S_CITY) == ' ' && *(pGeoRec+GEO_OFF_N_CITY) >= 'A')
   {
      iFixCity++;
      memcpy(acCity, pGeoRec+GEO_OFF_N_CITY, GEO_SIZ_CITY);
      acCity[GEO_SIZ_CITY] = 0;

      // Translate City
      City2Code(myTrim(acCity), acTmp);
      memcpy(pRec+OFF_S_CITY, acTmp, GEO_SIZ_CITY);

      // Copy zip code - Zip4 may not be accurate to take
      memcpy(pRec+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP);
      memcpy(pRec+OFF_S_ZIP4, "    ", 4);
      memcpy(pRec+OFF_S_ST, "CA", 2);
   } else if (*(pRec+OFF_S_CITY) >= '1')
   {
      // Remap city
      RemapCity(pRec);
   } else
   {
      // Just to be safe, blank it out
      memset(pRec+OFF_S_CITY, ' ', GEO_SIZ_CITY);
      return;
   }

   // Count number of records that use new address
   iChgCity++;
}

/*********************************** Sfx2Code *******************************
 *
 *
 *****************************************************************************

void Sfx2Code(char *pSuffix, char *pSfxCode)
{
   int iTmp;
 
   if (!*pSuffix || *pSuffix == ' ')
   {
      strcpy(pSfxCode, "     ");
      return;
   }

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      if (!strcmp(pSuffix, asSuffixTbl[iTmp]))
      {
         sprintf(pSfxCode, "%d     ", iTmp);
         break;
      }
   }

   if (iTmp > iNumSuffixes)
   {
      // Not found
      LogMsg0("Invalid suffix: %s", pSuffix);
      strcpy(pSfxCode, "     ");
      iBadSuffix++;
   }
}

/********************************** MergeSitus *******************************
 *
 * Return 0 if successful, >0 if error
 *
 * 20050301: Update situs address without checking change flag because we update
 *           geocode data quarterly and county data may change from month to month.
 * 20130501: Ignore change if StrNum is not available.
 * 20150831: If street name matches first 12 chars, use new name.  This will fixes
 *           problem of street name being chopped off.
 * 20171116  Blank out StrSub only if we are about to change (Fix SCL)
 * 20190408  Add Unit# to fix bug that caused by code change in 20150831.  This only
 *           affect street name longer than 12 bytes and has trailing unit#.
 * 20190420  Update S_UNIT only if it's blank (Test on CCX)
 * 20220621  Clear S_FRA if matching Centrus's S_UNIT.
 * 20240320  If POST_DIR is present, update situs.
 *
 *****************************************************************************/

int MergeSitus(char *pGeoRec, char *pOutbuf)
{
   char     acCity[GEO_SIZ_CITY*2], acSuffix[GEO_SIZ_SUFFIX*2], acStrSub[8];
   char     acTmp[_MAX_PATH], acAddr2[_MAX_PATH], *pTmp;
   GEO512   *pRec = (GEO512 *)pGeoRec;
   bool     bNoChg=false;
   int      iStrNum, iTmp;

   // Ignore situs if requested
   if (bIgnoreSitus)
      return 0;

   acStrSub[0] = 0;
   iStrNum = 0;

   // If new city is blank, the record is not geocoded; populate it with old city name
   if (*(pGeoRec+GEO_OFF_N_CITY) <= '0')
   {
      // Use old city & zip 08/24/2006 spn
      // Populate only if current city is blank 02/25/2009 spn
      if (*(pGeoRec+GEO_OFF_CITY) >= 'A' && *(pOutbuf+OFF_S_CITY) == ' ')
      {
         memcpy(acCity, pGeoRec+GEO_OFF_CITY, GEO_SIZ_CITY);
         myTrim(acCity,GEO_SIZ_CITY);
         City2Code(acCity, acTmp);
         if (acTmp[0] > ' ')
         {
            if (bDebug && memcmp(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY))
               LogMsg0("+++ 1. City changes from %.3s to %.3s ->%.14s", pOutbuf+OFF_S_CITY, acTmp, pOutbuf);

            memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
            if (*(pOutbuf+OFF_S_CTY_ST_D) == ' ')
            {
               memcpy(pOutbuf+OFF_S_ST, "CA", 2);
               sprintf(acTmp, "%s, CA", acCity);
               blankPad(acTmp, SIZ_S_CTY_ST_D);
               memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
            }
         }
      }
      if (*(pGeoRec+GEO_OFF_ZIP) >= '0' && *(pOutbuf+OFF_S_ZIP) == ' ')
         memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_ZIP, SIZ_S_ZIP);
   } else
   {
      // Only populate addr1 if original is blank.  This idea won't work since
      // we want to use Centrus to correct our addresses.  This will fix DNX problem
      // where county provides non-standard addresses.  Take out on 8/29/2008 -spn
      // If the first 12 bytes are the same, use Centrus address (RIV 669522015). -spn
      // 01/04/2023 - Special case in RIV (245 W VISTA ROYALE CIR --> 245 VISTA ROYALE CIR W)
      if (*(pOutbuf+OFF_S_STREET) == ' ' || bUseGeoAdr1 || !memcmp(pOutbuf+OFF_S_STREET, pRec->acStreet, 12) )
      {
         pTmp = pGeoRec+GEO_OFF_N_HOUSENUM;
         int iSub=0;
         iTmp=0;

         // Update StrNum if original is blank 20161117
         iStrNum = atoin(pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM); 
         if (!iStrNum && bUseGeoSNum)
         {
            while (pTmp < pGeoRec+GEO_OFF_N_HOUSENUM+GEO_SIZ_N_HOUSENUM)
            {
               if (isalpha(*pTmp))
               {
                  acStrSub[iSub++] = *pTmp;
                  *pTmp = ' ';                  // replace alpha with space
               }

               // Keep first number only 20161117
               if (*pTmp == '-')
                  break;

               acTmp[iTmp++] = *pTmp;
               pTmp++;
            }
            acTmp[iTmp] = 0;

            iStrNum = atoin(acTmp, SIZ_S_STRNUM); 
         } else
            acTmp[0] = 0;

         // If no StrNum, don't update
         if (iStrNum > 0)
         {
            if (pTmp=strchr(acTmp, '/'))
            {
               // Blank out StrSub only if we are about to change
               memset(pOutbuf+OFF_S_STR_SUB, ' ', SIZ_S_STR_SUB);
               pTmp -= 2;
               if (*pTmp == ' ')
               {
                  // StrSub found - if sub is cut off, don't take it
                  *pTmp++ = 0;
                  if (strlen(pTmp) == SIZ_S_STR_SUB)
                     memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, SIZ_S_STR_SUB);
               }
            }

            if (pRec->acMCode[0] == 'S' && (pRec->acMCode[2] & CHG_STREET))
               LogMsg0("*** 2. Street name changed [%.*s]: %.24s to %.40s --> Use it", iApnLen, pRec->origAdr.acApn, pRec->origAdr.acStrName, pRec->acStreet);

            // Replace StrName only if it contains more than two character - 20110421
            memcpy(acTmp, pOutbuf+OFF_S_STREET, SIZ_S_STREET);
            myTrim(acTmp, SIZ_S_STREET);
            iTmp = strlen(acTmp);
            if (iTmp > 2 || acTmp[0] == 0)
            {
               // Left justify StrNum
               sprintf(acTmp, "%d       ", iStrNum);
               if (memcmp(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM))
               {
                  LogMsg0("1. StrNum changed [%.*s]: %.7s to %s", iApnLen, pRec->origAdr.acApn, pRec->origAdr.acStrNum, acTmp);
                  iChgStrNum++;
               }

               // 06/25/2018 spn - fix SJX - always keeps StrNum from county, Centrus cannot change it. Or if original is blank
               // 07/23/2019 spn - only update if GSNum=Y in INI file
               if (bUseGeoSNum && *(pOutbuf+OFF_S_STRNUM) == ' ')
               {
                  memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
                  memcpy(pOutbuf+OFF_S_HSENO, acTmp, SIZ_S_STRNUM);
               }

               // 01/05/2023 Use post DIR if available
               if (*(pGeoRec+GEO_OFF_N_PREDIR) > ' ')
                  memcpy(pOutbuf+OFF_S_DIR, pGeoRec+GEO_OFF_N_PREDIR, GEO_SIZ_DIR);
               else if (*(pGeoRec+GEO_OFF_N_POSTDIR) > ' ')
                  memcpy(pOutbuf+OFF_S_DIR, pGeoRec+GEO_OFF_N_POSTDIR, GEO_SIZ_DIR);

               if (bUseGeoSStr)
               {
                  memcpy(pOutbuf+OFF_S_STREET, pGeoRec+GEO_OFF_N_STREET, GEO_SIZ_STREET);
                  if (pRec->acHseFra[0] > ' ')
                     memcpy(pOutbuf+OFF_S_STR_SUB, pRec->acHseFra, GEO_SIZ_N_HOUSEFRA);
                  else if (iSub > 0)
                     memcpy(pOutbuf+OFF_S_STR_SUB, acStrSub, iSub);

#ifdef _DEBUG
                  //if (!memcmp(pOutbuf, "006520016000", 9) )
                  //   iTmp = 0;
#endif
                  // Unit# (add 4/8/2019), Add bUseGeoUnit 7/23/2019
                  if (bUseGeoUnit && *(pOutbuf+OFF_S_UNITNO) == ' ')
                  {
                     if (*(pOutbuf+OFF_S_STR_SUB) > ' ' && (*(pOutbuf+OFF_S_STR_SUB) == *(pGeoRec+GEO_OFF_N_UNIT)) )
                        memset(pOutbuf+OFF_S_STR_SUB, ' ', SIZ_S_STR_SUB);
                     if ( *(pGeoRec+GEO_OFF_UNIT) > ' ')
                        memcpy(pOutbuf+OFF_S_UNITNO, pGeoRec+GEO_OFF_UNIT, GEO_SIZ_UNIT);
                     else if ( *(pGeoRec+GEO_OFF_N_UNIT) > ' ')
                        memcpy(pOutbuf+OFF_S_UNITNO, pGeoRec+GEO_OFF_N_UNIT, GEO_SIZ_UNIT);
                  }
                  // Fix SDX 7/23/2019
                  if (bUseGeoUnit && bUseGeoSNum)
                     memcpy(pOutbuf+OFF_S_ADDR_D, pGeoRec+GEO_OFF_N_ADDRESS, GEO_SIZ_ADDR1);

                  if (*(pOutbuf+OFF_S_SUFF) == ' ' && *(pGeoRec+GEO_OFF_N_STRSUF) > ' ')
                  {
                     // Our data has 5-byte but GeoRecs has only 4 bytes
                     memcpy(acSuffix, pGeoRec+GEO_OFF_N_STRSUF, GEO_SIZ_N_STRSUF);
                     acSuffix[GEO_SIZ_N_STRSUF] = 0;

                     // Translate suffix
                     Sfx2Code(myTrim(acSuffix), acTmp);
                     memcpy(pOutbuf+OFF_S_SUFF, acTmp, GEO_SIZ_SUFFIX);

                     iChgSfx++;
                  }
               }
            } else
               bNoChg = true;

            // Count number of records that use new address
            iChgStr++;
         }
      } else
         iStrNum = atoin(pGeoRec+GEO_OFF_N_HOUSENUM, GEO_SIZ_N_HOUSENUM);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "006520016000", 8) )
      //   iTmp = 0;
#endif
      // Use geocoded city if requested 2/25/2015
      if (bUseGeoCity)
      {
         memcpy(acCity, pGeoRec+GEO_OFF_N_CITY, GEO_SIZ_CITY);
         myTrim(acCity, GEO_SIZ_CITY);
         City2Code(acCity, acTmp);
         if (acTmp[0] > ' ')
         {
            if (bDebug && *(pOutbuf+OFF_S_CITY) > ' ' && memcmp(pOutbuf+OFF_S_CITY, acTmp, 3))
               LogMsg("1.### City changed: %.3s --> %.3s APN=%.12s", pOutbuf+OFF_S_CITY, acTmp, pOutbuf);

            memcpy(pOutbuf+OFF_S_CITY, acTmp, GEO_SIZ_CITY);
            memcpy(pOutbuf+OFF_S_ST, "CA", 2);
            if (*(pGeoRec+GEO_OFF_N_ZIP) > '0' )
            {
               // Update Zip4 if available
               memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
            }

            if (*(pOutbuf+OFF_S_ZIP4) > ' ')
               sprintf(acAddr2, "%s, CA %.5s-%.4s", acCity, pOutbuf+OFF_S_ZIP, pOutbuf+OFF_S_ZIP4);
            else
               sprintf(acAddr2, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
            blankPad(acAddr2, SIZ_S_CTY_ST_D);  // Pad with blank
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
         }
      }      
      else if (*(pGeoRec+GEO_OFF_CITY) >= 'A' && iStrNum > 0)     // Has city, fill in zipcode
      {
         // Translate City - Use existing city as county provided
         memcpy(acCity, pGeoRec+GEO_OFF_CITY, GEO_SIZ_CITY);
         myTrim(acCity, GEO_SIZ_CITY);

         // 05/17/2022
         //City2Code(acCity, acTmp);
         //if (bDebug && memcmp(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY))
         //{
         //   LogMsg0("+++ 2. City changes from %.3s to %.3s ->%.14s", pOutbuf+OFF_S_CITY, acTmp, pOutbuf);
         //   iChgCity++;
         //}
         //memcpy(pOutbuf+OFF_S_CITY, acTmp, GEO_SIZ_CITY);
         //memcpy(pOutbuf+OFF_S_ST, "CA", 2);

         // Fill in zip if blank and ignore zip flag is false
         if (*(pOutbuf+OFF_S_ZIP) == ' ' && !bIgnoreZip) 
         {
            if (bChkCity)
            {
               // If different city name, ignore zipcode
               if (!memcmp(acCity, pGeoRec+GEO_OFF_N_CITY, strlen(acCity)))
               {
                  memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
                  iChgZip++;
               }
            } else
            {
               memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
               iChgZip++;
            }
         } else if (*(pGeoRec+GEO_OFF_N_ZIP) > '0' && !memcmp(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP))
         {
            // Update Zip4 if available
            memcpy(pOutbuf+OFF_S_ZIP4, pGeoRec+GEO_OFF_N_ZIP4, GEO_SIZ_ZIP4);
            iChgZip4++;
         } else if (bUseZip && memcmp(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP))
         {
            LogMsg0("+++ 3. Zip changes from %.5s to %.5s ->%.14s", pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, pOutbuf);
            memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
            iChgZip++;
         }

         if (*(pOutbuf+OFF_S_ZIP4) > ' ')
            sprintf(acAddr2, "%s, CA %.5s-%.4s", acCity, pOutbuf+OFF_S_ZIP, pOutbuf+OFF_S_ZIP4);
         else
            sprintf(acAddr2, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
         blankPad(acAddr2, SIZ_S_CTY_ST_D);  // Pad with blank
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      } 
      else if (*(pGeoRec+GEO_OFF_N_CITY) >= 'A' && !bIgnoreCity && iStrNum > 0)    // No city name, fill in new one
      {
         // When update city, translate to code first
         memcpy(acCity, pGeoRec+GEO_OFF_N_CITY, GEO_SIZ_CITY);
         myTrim(acCity, GEO_SIZ_CITY);
         City2Code(acCity, acTmp);

         memcpy(pOutbuf+OFF_S_CITY, acTmp, GEO_SIZ_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);

         iChgCity++;

         // If zipcode is provided, do not change
         if (*(pOutbuf+OFF_S_ZIP) == ' ')
            memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);

         if (*(pOutbuf+OFF_S_ZIP4) > ' ')
            sprintf(acAddr2, "%s, CA %.5s-%.4s", acCity, pOutbuf+OFF_S_ZIP, pOutbuf+OFF_S_ZIP4);
         else
            sprintf(acAddr2, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);

         blankPad(acAddr2, SIZ_S_CTY_ST_D);  // Pad with blank
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      } 
      else
      {
         // Leave it as is.  Blank them only if requested
         if (bClrCity)
         {
            memset(pOutbuf+OFF_S_CITY, ' ', GEO_SIZ_CITY);
            memset(pOutbuf+OFF_S_CTY_ST_D, ' ', GEO_SIZ_ADDR2);
         }
      }
   }

   // Update mailing with situs
   if (bSitusOnly)
   {
      // Check to see if mailing avail, if not update mailing with situs
      if (*(pOutbuf+OFF_M_STREET) < '0')
      {
         iUpdMail++;
         memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
         memcpy(pOutbuf+OFF_M_STR_SUB, pOutbuf+OFF_S_STR_SUB, SIZ_S_STR_SUB);
         memcpy(pOutbuf+OFF_M_DIR, pGeoRec+GEO_OFF_N_PREDIR, GEO_SIZ_DIR);
         memcpy(pOutbuf+OFF_M_STREET, pGeoRec+GEO_OFF_N_STREET, GEO_SIZ_STREET);
         memcpy(pOutbuf+OFF_M_SUFF, pGeoRec+GEO_OFF_N_STRSUF, GEO_SIZ_N_STRSUF);
         if (acCity[0] >= 'A')
         {
            blankPad(acCity, SIZ_M_CITY);
            memcpy(pOutbuf+OFF_M_CITY, acCity, SIZ_M_CITY);
         }
         memcpy(pOutbuf+OFF_M_ST, "CA", 2);
         memcpy(pOutbuf+OFF_M_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
      }
   }
   return 0;
}

/******************************* MergeOvrSitus *******************************
 *
 * Overwrite situs with data from geocoded file
 *
 *****************************************************************************/

int MergeOvrSitus(char *pGeoRec, char *pOutbuf)
{
   char     acCity[GEO_SIZ_CITY*2], acSuffix[GEO_SIZ_SUFFIX*2];
   char     acTmp[_MAX_PATH];
   GEO512   *pRec = (GEO512 *)pGeoRec;
   int      iTmp;

   if (*(pGeoRec+GEO_OFF_N_CITY) > '0')
   {
      memcpy(pOutbuf+OFF_S_ADDR_D, pGeoRec+GEO_OFF_N_ADDRESS, GEO_SIZ_ADDR1);
      memcpy(acTmp, pGeoRec+GEO_OFF_N_LASTLINE, SIZ_S_CTY_ST_D);
      iTmp = blankRem(acTmp, SIZ_S_CTY_ST_D);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);

      memcpy(pOutbuf+OFF_S_STRNUM, pGeoRec+GEO_OFF_N_HOUSENUM, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, pGeoRec+GEO_OFF_N_HOUSENUM, SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_STR_SUB, pGeoRec+GEO_OFF_N_HOUSEFRA, SIZ_S_STR_SUB);
      memcpy(pOutbuf+OFF_S_DIR, pGeoRec+GEO_OFF_N_PREDIR, SIZ_S_DIR);
      memcpy(pOutbuf+OFF_S_STREET, pGeoRec+GEO_OFF_N_STREET, SIZ_S_STREET);
      memcpy(pOutbuf+OFF_S_UNITNO, pGeoRec+GEO_OFF_UNIT, SIZ_S_UNITNO);

      // Our data has 5-byte but GeoRecs has only 4 bytes
      memcpy(acSuffix, pGeoRec+GEO_OFF_N_STRSUF, GEO_SIZ_N_STRSUF);
      acSuffix[GEO_SIZ_N_STRSUF] = 0;

      // Translate suffix
      Sfx2Code(myTrim(acSuffix), acTmp);
      vmemcpy(pOutbuf+OFF_S_SUFF, acTmp, GEO_SIZ_SUFFIX);

      memcpy(acCity, pGeoRec+GEO_OFF_N_CITY, GEO_SIZ_CITY);
      myTrim(acCity, GEO_SIZ_CITY);
      City2Code(acCity, acTmp);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, 3);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         if (*(pGeoRec+GEO_OFF_N_ZIP) > '0' )
            memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
      }
   }

   return 0;
}

/******************************** MergeCityZip *******************************
 *
 * Merge City & Zipcode only when county data is blank.
 *
 *****************************************************************************/

int MergeCityZip(char *pGeoRec, char *pOutbuf)
{
   char  acCity[GEO_SIZ_CITY*2];
   char  acTmp[_MAX_PATH], acAddr2[_MAX_PATH];

   // Ignore situs if requested
   if (bIgnoreSitus)
      return 0;

   // Use geocoded city if requested 2/25/2015
   if (bUseGeoCity)
   {
      memcpy(acCity, pGeoRec+GEO_OFF_N_CITY, GEO_SIZ_CITY);
      myTrim(acCity, GEO_SIZ_CITY);
      City2Code(acCity, acTmp);
      if (acTmp[0] > ' ')
      {
         if (bDebug && *(pOutbuf+OFF_S_CITY) > ' ' && memcmp(pOutbuf+OFF_S_CITY, acTmp, 3))
            LogMsg("2.### City changes: %.3s --> %.3s APN=%.12s", pOutbuf+OFF_S_CITY, acTmp, pOutbuf);

         memcpy(pOutbuf+OFF_S_CITY, acTmp, GEO_SIZ_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         if (*(pGeoRec+GEO_OFF_N_ZIP) > '0' )
         {
            // Update Zip4 if available
            memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
         }

         if (*(pOutbuf+OFF_S_ZIP4) > ' ')
            sprintf(acAddr2, "%s, CA %.5s-%.4s", acCity, pOutbuf+OFF_S_ZIP, pOutbuf+OFF_S_ZIP4);
         else
            sprintf(acAddr2, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
         blankPad(acAddr2, SIZ_S_CTY_ST_D);  // Pad with blank
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      }
   } 
   // Translate City - Use existing city as county provided
   else if (*(pGeoRec+GEO_OFF_CITY) >= 'A')
   {
      memcpy(acCity, pGeoRec+GEO_OFF_CITY, GEO_SIZ_CITY);
      myTrim(acCity, GEO_SIZ_CITY);
      City2Code(acCity, acTmp);

      if (bDebug && memcmp(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY))
         LogMsg0("+++ 2. City changes from %.3s to %.3s ->%.14s", pOutbuf+OFF_S_CITY, acTmp, pOutbuf);
      
      memcpy(pOutbuf+OFF_S_CITY, acTmp, GEO_SIZ_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      // Fill in zip if blank and ignore zip flag is false
      if (*(pOutbuf+OFF_S_ZIP) == ' ' && !bIgnoreZip) 
      {
         if (bChkCity)
         {
            // If different city name, ignore zipcode
            if (!memcmp(acCity, pGeoRec+GEO_OFF_N_CITY, strlen(acCity)))
            {
               memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
               iChgZip++;
            }
         } else
         {
            memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
            iChgZip++;
         }
      } else if (*(pGeoRec+GEO_OFF_N_ZIP) > '0' &&
         !memcmp(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP))
      {
         // Update Zip4 if available
         memcpy(pOutbuf+OFF_S_ZIP4, pGeoRec+GEO_OFF_N_ZIP4, GEO_SIZ_ZIP4);
         iChgZip4++;
      }

      if (*(pOutbuf+OFF_S_ZIP4) > ' ')
         sprintf(acAddr2, "%s, CA %.5s-%.4s", acCity, pOutbuf+OFF_S_ZIP, pOutbuf+OFF_S_ZIP4);
      else
         sprintf(acAddr2, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
      blankPad(acAddr2, SIZ_S_CTY_ST_D);  // Pad with blank
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
   } else if (*(pGeoRec+GEO_OFF_N_CITY) >= 'A' && !bIgnoreCity)
   {
      // When update city, translate to code first
      memcpy(acCity, pGeoRec+GEO_OFF_N_CITY, GEO_SIZ_CITY);
      myTrim(acCity, GEO_SIZ_CITY);
      City2Code(acCity, acTmp);

      if (bDebug && memcmp(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY))
         LogMsg0("+++ 3. City changes from %.3s to %.3s ->%.14s", pOutbuf+OFF_S_CITY, acTmp, pOutbuf);

      memcpy(pOutbuf+OFF_S_CITY, acTmp, GEO_SIZ_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      iChgCity++;

      memcpy(pOutbuf+OFF_S_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
      memcpy(pOutbuf+OFF_S_ADDR_D, pGeoRec+GEO_OFF_N_ADDRESS, GEO_SIZ_ADDR1);
      memcpy(acTmp, pGeoRec+GEO_OFF_N_LASTLINE, GEO_SIZ_ADDR2);
      blankRem(acTmp, GEO_SIZ_ADDR2);  // Remove extra spaces in the middle
      blankPad(acTmp, SIZ_S_CTY_ST_D); // Pad with blank
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   } else
   {
      // Leave it as is.  Blank them only if requested
      if (bClrCity)
      {
         memset(pOutbuf+OFF_S_CITY, ' ', GEO_SIZ_CITY);
         memset(pOutbuf+OFF_S_CTY_ST_D, ' ', GEO_SIZ_ADDR2);
      }
   }

   // Update mailing with situs
   if (bSitusOnly)
   {
      // Check to see if mailing avail, if not update mailing with situs
      if (*(pOutbuf+OFF_M_STREET) < '0')
      {
         iUpdMail++;
         memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
         memcpy(pOutbuf+OFF_M_STR_SUB, pOutbuf+OFF_S_STR_SUB, SIZ_S_STR_SUB);
         memcpy(pOutbuf+OFF_M_DIR, pGeoRec+GEO_OFF_N_PREDIR, GEO_SIZ_DIR);
         memcpy(pOutbuf+OFF_M_STREET, pGeoRec+GEO_OFF_N_STREET, GEO_SIZ_STREET);
         memcpy(pOutbuf+OFF_M_SUFF, pGeoRec+GEO_OFF_N_STRSUF, GEO_SIZ_N_STRSUF);
         if (acCity[0] >= 'A')
         {
            blankPad(acCity, SIZ_M_CITY);
            memcpy(pOutbuf+OFF_M_CITY, acCity, SIZ_M_CITY);
         }
         memcpy(pOutbuf+OFF_M_ST, "CA", 2);
         memcpy(pOutbuf+OFF_M_ZIP, pGeoRec+GEO_OFF_N_ZIP, GEO_SIZ_ZIP+GEO_SIZ_ZIP4);
      }
   }
   return 0;
}

/********************************** MakeNewFields ****************************
 *
 *
 *****************************************************************************/

void MakeNewFields(char *pGeoRec, char *pOutbuf)
{
   char acLongLat[32], acLong[16], acLat[16];
   char acCR[8], acCensus[16], acDpbc[4];

   *pOutbuf = 0;

   // Convert long/lat
   if (*(pGeoRec+GEO_OFF_N_LON) > ' ')
   {
      memcpy(acLong, pGeoRec+GEO_OFF_N_LON, GEO_SIZ_N_LON);
      acLong[GEO_SIZ_N_LON] = 0;
      memcpy(acLat, pGeoRec+GEO_OFF_N_LAT, GEO_SIZ_N_LAT);
      acLat[GEO_SIZ_N_LAT] = 0;
      GeoToLong(acLong, acLat, acLongLat);
   } else
      strcpy(acLongLat, "                   ");    // 19 spaces

   // Get CR
   if (bIgnoreSitus)
      memset(acCR, ' ', GEO_SIZ_N_CARRT);
   else
      memcpy(acCR, pGeoRec+GEO_OFF_N_CARRT, GEO_SIZ_N_CARRT);

   acCR[GEO_SIZ_N_CARRT] = 0;
   // Get Census tract:  We only use 6 digit.  Ignore the rest
   if (*(pGeoRec+GEO_OFF_N_TRACT+5) > ' ' && memcmp(pGeoRec+GEO_OFF_N_TRACT+5, "0000", 4))
   {
      int   iTmp1, iTmp2;
      char  cBlk;

      iTmp1 = atoin(pGeoRec+GEO_OFF_N_TRACT+5, 4);
      if (*(pGeoRec+GEO_OFF_N_TRACT+9) == '.')
      {  // Use Centrus Desktop
         iTmp2 = atoin(pGeoRec+GEO_OFF_N_TRACT+10, 2);
         cBlk = *(pGeoRec+GEO_OFF_N_TRACT+12);
      } else
      {  // Use GeoStan
         iTmp2 = atoin(pGeoRec+GEO_OFF_N_TRACT+9, 2);
         cBlk = *(pGeoRec+GEO_OFF_N_TRACT+11);
      }
      sprintf(acCensus, "%d.%.2d/%c   ", iTmp1, iTmp2, cBlk);
   } else
      memcpy(acCensus, "            ", GEO_SIZ_CENSUS_BLK);
   acCensus[GEO_SIZ_CENSUS_BLK] = 0;

   memcpy(acDpbc, pGeoRec+GEO_OFF_N_DPBC, GEO_SIZ_N_DPBC);
   acDpbc[GEO_SIZ_N_DPBC] = 0;

   // Merge CR, long/lat, Census Tract, DPBC
   sprintf(pOutbuf, "%s%s%s%s", acCR, acLongLat, acCensus, acDpbc);
}

/******************************** MakeGuessFields ****************************
 *
 * We can only guess location on LAT/LONG and Census Tract.  Other info
 * may not apply since address is not geocodable.
 *
 *****************************************************************************/

void MakeGuessFields(char *pGeoRec, char *pOutbuf)
{
   char acLongLat[32], acLong[16], acLat[16];
   char acCR[8], acCensus[16], acDpbc[4];

   *pOutbuf = 0;

   // Convert long/lat
   memcpy(acLong, pGeoRec+GEO_OFF_N_LON, GEO_SIZ_N_LON);
   acLong[GEO_SIZ_N_LON] = 0;
   memcpy(acLat, pGeoRec+GEO_OFF_N_LAT, GEO_SIZ_N_LAT);
   acLat[GEO_SIZ_N_LAT] = 0;
   GeoToLong(acLong, acLat, acLongLat);

   // Ignore long/lat, copy other fields 01/03/2018 spn
   memset(acLongLat, ' ', 19);
   acLongLat[19] = 0;

   memcpy(acCR, "    ", GEO_SIZ_N_CARRT);
   acCR[GEO_SIZ_N_CARRT] = 0;
   if (*(pGeoRec+GEO_OFF_N_TRACT+5) > ' ' && memcmp(pGeoRec+GEO_OFF_N_TRACT+5, "0000", 4))
   {
      int   iTmp1, iTmp2;
      char  cBlk;

      iTmp1 = atoin(pGeoRec+GEO_OFF_N_TRACT+5, 4);
      if (*(pGeoRec+GEO_OFF_N_TRACT+9) == '.')
      {  // Use Centrus Desktop
         iTmp2 = atoin(pGeoRec+GEO_OFF_N_TRACT+10, 2);
         cBlk = *(pGeoRec+GEO_OFF_N_TRACT+12);
      } else
      {  // Use GeoStan
         iTmp2 = atoin(pGeoRec+GEO_OFF_N_TRACT+9, 2);
         cBlk = *(pGeoRec+GEO_OFF_N_TRACT+11);
      }
      sprintf(acCensus, "%d.%.2d/%c   ", iTmp1, iTmp2, cBlk);      
   } else
      memcpy(acCensus, "            ", GEO_SIZ_CENSUS_BLK);
   acCensus[GEO_SIZ_CENSUS_BLK] = 0;

   memcpy(acDpbc, "  ", GEO_SIZ_N_DPBC);
   acDpbc[GEO_SIZ_N_DPBC] = 0;

   // Merge CR, long/lat, Census Tract, DPBC
   sprintf(pOutbuf, "%s%s%s%s", acCR, acLongLat, acCensus, acDpbc);
}

/************************************ LoadCities *****************************
 *
 * Initialize global variables
 *
 *****************************************************************************

int LoadCities(char *pCityFile)
{
   char  acTmp[_MAX_PATH], *pTmp;
   int   iRet=0, iTmp;
   FILE  *fd;

   fd = fopen(pCityFile, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iRet = atoi(acTmp);
      if (iRet > MAX_CITIES)
      {
         LogMsg("Please change MAX_CITIES in CityInfo.h to %d then rerun", iRet+1);
         fclose(fd);
         return 0;
      }

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp <= iRet-1; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && *pTmp >= '0')
         {
            pTmp = strtok(acTmp, ",");
            strcpy(myCityList[iTmp].acCityName, pTmp);

            // Retrieve old cityID
            pTmp = strtok(NULL, ",");
            if (!pTmp)
            {
               LogMsg("Bad old cityID for %s.  Please review city file %s", myCityList[iTmp].acCityName, pCityFile);
               break;
            }
            myCityList[iTmp].iOldCityID = atoi(pTmp);

            // Retrieve new cityID
            pTmp = strtok(NULL, ",");
            if (!pTmp)
            {
               LogMsg("Bad new cityID for %s.  Please review city file %s", myCityList[iTmp].acCityName, pCityFile);
               break;
            }
            myCityList[iTmp].iNewCityID = atoi(pTmp);
         } else
            break;
      }

      if (iTmp != iRet)
      {
         LogMsg("Bad number of city loaded %d.  Please verify %s", iTmp, pCityFile);
         iRet = 0;
      }

      fclose(fd);
   } else
      LogMsg("Error opening city table %s", pCityFile);

   return iRet-1;
}

/******************************** LoadSuffixTbl ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************

int LoadSuffixTbl(char *pFilename)
{
   char  acTmp[_MAX_PATH], *pTmp;
   int   iRet=0, iTmp;
   FILE  *fd;

   fd = fopen(pFilename, "r");
   asSuffixTbl[0][0] = 0;              // Initialize first entry

   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iRet = atoi(acTmp);
      if (iRet < 10 || iRet >= MAX_SUFFIX)
      {
         LogMsg("Suffix table may be bad.  Please verify: %s", pFilename);
         fclose(fd);
         return 0;
      }

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp < iRet; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
            strcpy(asSuffixTbl[iTmp], myTrim(acTmp));
         else
            break;
      }
      if (iTmp != iRet)
      {
         LogMsg("Bad number of suffixes loaded %d.  Please verify %s", iTmp, pFilename);
         iRet = 0;
      }

      fclose(fd);
   } else
      LogMsg("Error opening suffix table %s", pFilename);

   return iRet-1;
}

/******************************** LoadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadCountyInfo(char *pCntyCode, char *pCntyTbl)
{
   char  acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS];
   int   iRet=0, iTmp;
   FILE  *fd;

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            if (!memcmp(acTmp, pCntyCode, 3))
            {
               iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
               if (iTmp > 0)
               {
                  //strcpy(myCounty.acCntyCode, pCntyCode);
                  strcpy(myCounty.acStdApnFmt, pFlds[FLD_APN_FMT]);
                  strcpy(myCounty.acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
                  strcpy(myCounty.acCase[0], pFlds[FLD_SPC_BOOK]);
                  if (iTmp > FLD_YR_ASSD+2)
                  {
                     strcpy(myCounty.acSpcApnFmt[1], pFlds[FLD_SPC_FMT+2]);
                     strcpy(myCounty.acCase[1], pFlds[FLD_SPC_BOOK+2]);
                  }

                  strcpy(myCounty.acCntyID, pFlds[FLD_CNTY_ID]);
                  strcpy(myCounty.acCntyName, pFlds[FLD_CNTY_NAME]);
                  iRet = atoi(myCounty.acCntyID);
                  myCounty.iCntyID = iRet;
                  sprintf(myCounty.acFipsCode, "06%.3d", iRet*2-1);
                  myCounty.iApnLen = atoi(pFlds[FLD_APN_LEN]);
                  myCounty.iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
                  myCounty.iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
                  myCounty.iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
                  iRet = 1;
               } else
               {
                  LogMsg("Bad county table file: %s", pCntyTbl);
               }

               break;
            }
         } else
            break;
      }

      fclose(fd);

      if (1 != iRet)
         LogMsg("County not found.  Please verify %s", pCntyTbl);
   } else
      LogMsg("Error opening county table %s", pCntyTbl);

   return iRet;
}

/********************************** MergeAdrInit *****************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int MergeAdrInit(char *pCntyCode)
{
   char  acTmp[_MAX_PATH], acTmp1[_MAX_PATH], acRawTmpl[_MAX_PATH],
         acCityFile[_MAX_PATH], acExt[4];

   int   iRet;

   _getcwd(acIniFile, _MAX_PATH);
   strcat(acIniFile, "\\MergeAdr.ini");

   // If not found INI file in working folder, check default location
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\MergeAdr.ini");

   printf("Initializing %s\n", acIniFile);

   GetIniString("System", "LogPath", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acLogFile, "%s\\Merge_%s.log", acTmp, pCntyCode);

    // Open log file
   open_log(acLogFile, "w");

   // Get DocPath
   GetIniString("System", "DocPath", "", acDocPath, _MAX_PATH, acIniFile);

   // Prepare in/out files
   GetIniString(pCntyCode, "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   if (!acRawTmpl[0])
      GetIniString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);

   // Prepare input extension
   sprintf(acTmp, "%c01", bExtType);
   sprintf(acRawFile, acRawTmpl, pCntyCode, pCntyCode, acTmp);

   //if (bUseS01)
   //   sprintf(acRawFile, acRawTmpl, pCntyCode, pCntyCode, "S01");
   //else
   //   sprintf(acRawFile, acRawTmpl, pCntyCode, pCntyCode, "R01");

   if (bSitusOnly)
      strcpy(acExt, "S01");
   else
      strcpy(acExt, "G01");

   GetIniString("Data", "TmpFile", "", acTmp1, _MAX_PATH, acIniFile);
   if (acTmp1[0] >= ' ' && !bSitusOnly)
      sprintf(acOutFile, acTmp1, pCntyCode, acExt);
   else
      sprintf(acOutFile, acRawTmpl, pCntyCode, pCntyCode, acExt);

   if (iLdrYear > 2000)
      sprintf(acTmp1, "_LDR%d", iLdrYear);
   else
      acTmp1[0] = 0;
   replStr(acRawFile, "[year]", acTmp1);
   replStr(acOutFile, "[year]", acTmp1);

   // Get debug flag - county specific
   iRet = GetPrivateProfileString(pCntyCode, "DEBUG", "", acTmp, 3, acIniFile);
   if (!iRet)
      GetPrivateProfileString("System", "DEBUG", "", acTmp, 3, acIniFile);

   if (toupper(acTmp[0]) == 'Y')
      bDebug = true;
   if (toupper(acTmp[0]) == 'N')
      bDebug = false;

   // Get Situs flag
   if (!bIgnoreSitus)
   {
      GetPrivateProfileString(pCntyCode, "Situs", "", acTmp, 3, acIniFile);
      if (toupper(acTmp[0]) == 'N')
         bIgnoreSitus = true;
      else
         bIgnoreSitus = false;
   }

   // Get City flag
   GetPrivateProfileString(pCntyCode, "IgnoreCity", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bIgnoreCity = true;
   else
      bIgnoreCity = false;

   // Get geocoded addr line1 flag - default false
   GetPrivateProfileString(pCntyCode, "GAdr1", "N", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'N')
      bUseGeoAdr1 = false;
   else
      bUseGeoAdr1 = true;

   // Get geocoded city flag - default false
   GetPrivateProfileString(pCntyCode, "GCity", "N", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bUseGeoCity = true;
   else
      bUseGeoCity = false;

   // Get geocoded Unit# - default true
   GetPrivateProfileString(pCntyCode, "GUnit", "Y", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bUseGeoUnit = true;
   else
      bUseGeoUnit = false;

   // Get geocoded StrNum - default true
   GetPrivateProfileString(pCntyCode, "GSNum", "Y", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bUseGeoSNum = true;
   else
      bUseGeoSNum = false;
   
   // Get geocoded StrNum - default true
   GetPrivateProfileString(pCntyCode, "GSStr", "Y", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bUseGeoSStr = true;
   else
      bUseGeoSStr = false;

   // Get Zip flag
   GetPrivateProfileString(pCntyCode, "IgnoreZip", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bIgnoreZip = true;
   else
      bIgnoreZip = false;

   GetPrivateProfileString(pCntyCode, "ClrCity", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bClrCity = true;
   else
      bClrCity = false;

   GetPrivateProfileString(pCntyCode, "USPSOnly", "Y", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bUSPSOnly = true;
   else
      bUSPSOnly = false;
   
   iRet = GetPrivateProfileString(pCntyCode, "USPSZip", "", acTmp, 3, acIniFile);
   if (iRet < 1)
      iRet = GetPrivateProfileString("Data", "USPSZip", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bUseZip = true;
   else
      bUseZip = false;

   GetPrivateProfileString(pCntyCode, "ChkCity", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bChkCity = true;
   else
      bChkCity = false;

   GetPrivateProfileString(pCntyCode, "FmtMapLink", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bReformatMapLink = true;

   // Create M_ADDR_D flag
   GetPrivateProfileString(pCntyCode, "MAdr", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bCreateMAdr = true;
   else
      bCreateMAdr = false;

   GetIniString("Data", "AdrFile", "", acAdrTmpl, _MAX_PATH, acIniFile);
   if (bRegion > '0')
      sprintf(acAdrFile, acAdrTmpl, "LAX");
   else
      sprintf(acAdrFile, acAdrTmpl, pCntyCode);

   if (acAdrFile[strlen(acAdrFile)-1] == '*')
   {
      // Check for GIS file - EsriGC output
      strcpy(acTmp, acAdrFile);
      strcpy((char *)&acTmp[strlen(acTmp)-1], "GIS");
      if (!_access(acTmp, 0))
         strcpy(acAdrFile, acTmp);
      else
      {
         // Now try FIX file - MatchBP output
         LogMsgD("*** WARNING: File not found %s.  Try .fix file\n", acTmp);
         strcpy(acTmp, acAdrFile);
         strcpy((char *)&acTmp[strlen(acTmp)-1], "FIX");
         if (!_access(acTmp, 0))
            strcpy(acAdrFile, acTmp);
         else
         {
            // Now try DAT file - Centrus output
            LogMsgD("*** WARNING: File not found %s.  Try .dat file\n", acTmp);
            strcpy((char *)&acAdrFile[strlen(acAdrFile)-1], "DAT");
            if (_access(acAdrFile, 0))
            {
               LogMsgD("***** ERROR: File not found %s\n", acAdrFile);
               return -1;
            }
         }
      }
   }

   iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, acIniFile);
   iGeoLen = GetPrivateProfileInt(pCntyCode, "GeoSize", 0, acIniFile);
   if (!iGeoLen)
      iGeoLen = GetPrivateProfileInt("Data", "GeoSize", 416, acIniFile);

   // Load suffix table
   GetIniString("System", "SuffixTbl", ".\\Suffix.txt", acTmp, _MAX_PATH, acIniFile);
   iNumSuffixes = LoadSuffixTbl(acTmp);
   
   // Load Map Index
   /*
   if (!memicmp(pCntyCode, "SLO", 3))
   {
      GetIniString(pCntyCode, "MAPLST", "", (char *)&acMLFile[0], _MAX_PATH, acIniFile);
      if (acMLFile[0] && !_access(acMLFile, 0))
      {
         LogMsg("Loading Map List... %s", acMLFile);
         iRet = loadMapIndex(pCntyCode, acMLFile);
         if (iRet < 0)
         {
            LogMsg("***** Error loading Map List for %s.  Please check!", pCntyCode);
            return iRet;
         }
      }
   }
   */

   // Skip warning
   GetIniString(pCntyCode, "Warning", "Y", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bWarning = true;
   else
      bWarning = false;

   // Overwrite situs
   GetIniString(pCntyCode, "Overwrite", "N", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bOverwriteSitus = true;

   // Format DocLink
   GetIniString(pCntyCode, "DocLink", "", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bFormatDocLink = true;

   // Check remove PQZoning flag
   GetIniString(pCntyCode, "RemPQZoning", "N", acTmp, 3, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bRemovePQZoning = true;
   
   // Get county info
   GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = LoadCountyInfo(pCntyCode, acTmp);
   if (iRet == 1)
   {
      // Load city table
      GetIniString("Data", "CityFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCityFile, acTmp, pCntyCode);
      LogMsgD("Loading city table: %s\n", acCityFile);
      iNumCities = Load_N2CC(acCityFile);
      iRet = iNumCities;
   }

   return iRet;
}

/************************************** Usage ********************************
 *
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: MergeAdr -C<CntyCode> [-I|-A] [F<option>] [-U[s|v]] [-Y<yyyy>] [-Sn (default 1)] [-W]\n");
   printf("    -I : ignore situs address (as for DNX)\n");
   printf("    -A : merge situs address only, no geocode info\n");
   printf("    -E : send email when error occurs\n");
   printf("    -W : overwrite situs (using data from gis file)\n");
   printf("    -Fm: reformat maplink\n");
   printf("    -Fa: reformat APN\n");
   printf("    -Fd: reformat DOCLINK\n");
   printf("    -Ursv: Use S01 as first input (default R01)\n");
   printf("    -Y<yyyy>: Use raw file with _LDRyyyy\n");
   printf("    -Sn: skip first n records\n\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;                // gotten option character
   char *pszParam, *pCnty;

   bReformatApn = false;
   bIgnoreSitus = false;
   bEnCode = true;
   bUseSfxXlat = false;
   bSitusOnly = false;
   bReformatMapLink = false;
   bFormatDocLink = false;
   //bUseS01 = false;
   bSendMail = false;
   bOverwriteSitus = false;
   iSkip = 1;
   bRegion = 0;
   iLdrYear = 0;
   bExtType = 'R';

   if (argv[1][0] != '-')
   {
      pCnty = argv[1];
      if (!memcmp(pCnty, "LA1", 3) || !memcmp(pCnty, "LA2", 3) || !memcmp(pCnty, "LA3", 3))
         bRegion = *(pCnty+2);
      strcpy(myCounty.acCntyCode, pCnty);
      return;
   }

   while (1)
   {
      chOpt = GetOption(argc, argv, "AEWF:C:S:U:Y:I?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'A':
               bSitusOnly = true;
               break;

            case 'E':
               bSendMail = true;
               break;

            case 'F':
               if (pszParam != NULL)
               {
                  if (*pszParam == 'm')
                     bReformatMapLink = true;
                  else if (*pszParam == 'a')
                     bReformatApn = true;
                  else if (*pszParam == 'd')
                     bFormatDocLink = true;
               } else
                  bReformatMapLink = true;
               break;

            case 'C':   // county code
               if (pszParam != NULL)
               {
                  if (!memcmp(pszParam, "LA1", 3) || !memcmp(pszParam, "LA2", 3) || !memcmp(pszParam, "LA3", 3))
                     bRegion = *(pszParam+2);
                  strcpy(myCounty.acCntyCode, pszParam);
               } else
                  Usage();
               break;

            case 'S':   // Skip records
               if (pszParam != NULL)
                  iSkip = atol(pszParam);
               else
                  Usage();
               break;

            case 'U':   // Use extension as input
               bOverwriteSitus = true;
               if (pszParam != NULL)
               {
                  bExtType = *pszParam;
                  //if (*pszParam == 's')
                  //   bUseS01 = true;
               } 
               break;

            case 'W':   // Use geocoding data to overwrite situs on R01
               bOverwriteSitus = true;
               break;

            case 'Y':   // LDR year
               if (pszParam != NULL)
                  iLdrYear = atol(pszParam);
               else
                  Usage();
               break;

            case 'I':   // Ignore situs address (DNX)
               bIgnoreSitus = true;
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         LogMsg("Argument [%s] not recognized\n", pszParam);
         break;
      }
   }
}

/*********************************** Mno_FixGeo *******************************
 *
 * Input/Output:  GIS file
 *
 ******************************************************************************/

int Mno_FixGeo(char *pCnty)
{
   char     acBuf[1024], acTmp[256], *pTmp;
   char     acOutFile[_MAX_PATH];

   FILE     *fdGeo, *fdOut;
   int      iRet;
   long     lCnt=0;

   LogMsgD("Fix APN format for %s", pCnty);
   LogMsgD("Open address file %s", acAdrFile);
   fdGeo = fopen(acAdrFile, "r");
   if (fdGeo == NULL)
   {
      LogMsgD("***** ERROR opening Geo file: %s\n", acAdrFile);
      return -2;
   }

   strcpy(acOutFile, acAdrFile);
   pTmp = strrchr(acOutFile, '.');
   strcpy(pTmp, ".out");
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsgD("***** ERROR opening output file: %s\n", acOutFile);
      return -3;
   }

   // Merge loop
   while (true)
   {
      iRet = fread(acBuf, 1, iGeoLen, fdGeo);

      // EOF ?
      if (iRet != iGeoLen)
         break;

      // Format APN
      pTmp = &acBuf[0];
      iRet = sprintf(acTmp, "0%.2s%.3s0%.2s%.3s ", pTmp, pTmp+2, pTmp+5, pTmp+7);
      memcpy(&acBuf[0], acTmp, iRet);

      iRet = fwrite(acBuf, 1, iGeoLen, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\n");

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fdGeo)
      fclose(fdGeo);

   // Rename output file
   strcpy(acTmp, acAdrFile);
   pTmp = strrchr(acTmp, '.');
   strcpy(pTmp, ".org");
   if (!_access(acTmp, 0))
      remove(acTmp);
   LogMsg("Rename %s to %s", acAdrFile, acTmp);
   iRet = rename(acAdrFile, acTmp);
   LogMsg("Rename %s to %s", acOutFile, acAdrFile);
   iRet = rename(acOutFile, acAdrFile);

   LogMsgD("Total output records:       %u", lCnt);

   return iRet;
}

/*********************************** Mno_FixGeo *******************************
 *
 * Translate APN in GIS file.
 *
 * Input:  GIS file and ApnXlat file (defined in INI file)
 * Output: TMP file.  User needs to resort or rename before use.
 *
 ******************************************************************************/

int ApnXlat(char *pCnty)
{
   char     acBuf[512], acApnRec[256], acTmp[32], *pTmp;
   char     acOutFile[_MAX_PATH], acApnFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   int      iRet, iTmp;
   long     lCnt=0;

   // Setup file names
   GetIniString(pCnty, "ApnXlat", "", acApnFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acAdrTmpl, pCnty, "TMP");
   if (acOutFile[strlen(acOutFile)-1] == '*')
      strcpy((char *)&acOutFile[strlen(acOutFile)-1], "TMP");

   LogMsg("Fix %s file for %s.  Translating APN", acAdrFile, pCnty);

   // Open R01 file
   LogMsg("Open R01 file %s", acAdrFile);
   fhIn = CreateFile(acAdrFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acAdrFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Open Apn file
   LogMsg("Open Apn file %s", acApnFile);
   FILE *fdApn = fopen(acApnFile, "r");
   if (fdApn == NULL)
   {
      LogMsg("***** Error opening Apn file: %s\n", acApnFile);
      return 2;
   }
   // Get 1st rec
   pTmp = fgets((char *)&acApnRec[0], 256, fdApn);

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iGeoLen);

   // Merge loop
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iGeoLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acAdrFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iGeoLen != nBytesRead)
         break;

NextApnRec:
      iTmp = memcmp(acBuf, (char *)&acApnRec[2], 13);
      if (!iTmp)
      {
         memcpy(acBuf, (char *)&acApnRec[32], 12);
         acBuf[12] = ' ';
         pTmp = &acBuf[0];

         // Read next 
         pTmp = fgets(acApnRec, 256, fdApn);

         if (!pTmp)
            bEof = true;
      } else if (iTmp > 0)
      {
         // Get next roll record
         pTmp = fgets(acApnRec, 256, fdApn);

         if (!pTmp)
            bEof = true;
         else
            goto NextApnRec;
      } else
      {
         pTmp = &acBuf[0];

         // Format APN
         iRet = sprintf(acTmp, "0%.2s%.3s0%.2s%.3s ", pTmp, pTmp+2, pTmp+5, pTmp+7);
         memcpy(acBuf, acTmp, iRet);
      }

      bRet = WriteFile(fhOut, acBuf, iGeoLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Do the rest of the file
   while (bRet && iGeoLen == nBytesRead)
   {
      bRet = ReadFile(fhIn, acBuf, iGeoLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acAdrFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iGeoLen != nBytesRead)
         break;

      pTmp = &acBuf[0];
      iRet = sprintf(acTmp, "0%.2s%.3s0%.2s%.3s ", pTmp, pTmp+2, pTmp+5, pTmp+7);
      memcpy(acBuf, acTmp, iRet);

      bRet = WriteFile(fhOut, acBuf, iGeoLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   printf("\n");

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);
   if (fdApn)
      fclose(fdApn);

   // Resort output file
   //sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   //lCnt = sortFile(acOutFile, acRawFile, "S(1,12,C,A) F(FIX,1900) BYPASS(1900,R)");

   LogMsgD("Total output records:       %u", lCnt);

   return 0;
}

/*********************************** copyStrNum ******************************
 *
 * Copy STRNUM to HSENO if blank
 *
 *****************************************************************************/

void copyStrNum(char *pOutbuf)
{
   long lStrNum;
   char sTmp[32];

   lStrNum = atoin(pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
   if (*(pOutbuf+OFF_S_HSENO) == ' ' && lStrNum > 0)
   {
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(sTmp, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      myLTrim(sTmp, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, sTmp, strlen(sTmp));
   }
}

/************************************** main *********************************
 *
 *
 *****************************************************************************/

int main(int argc, char* argv[])
{
   char     *pTmp, acBuf[MAX_RECSIZE], acGeoRec[GEO_REC_MAX], acGeoSave[GEO_REC_MAX], acVersion[64];
   char     acTmp[256], acTmp1[256], cFileCnt=1;
   char     acSubj[_MAX_PATH], acBody[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdGeo;

   int      iRet, iTmp;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iRet = LoadString(theApp.m_hInstance, IDS_APPVERSION, acVersion, 64);
   iRet = LoadString(theApp.m_hInstance, IDS_BUILDDATE, acTmp, 64);
   printf("%s - %s\n\n", acVersion, acTmp);

   if (argc < 2)
      Usage();

   // Parse command line
   ParseCmd(argc, argv);

   // Initialize county
   iTmp = MergeAdrInit(myCounty.acCntyCode);
   if (iTmp < 1)
   {
      LogMsgD("***** Error initializing MergeAdr\n");
      if (!iTmp)
         LogMsg("??? Please check city table for number of entries\n");

      if (bSendMail)
      {
         sprintf(acSubj, "***** %s - Error initializing MergeAdr.", myCounty.acCntyCode);
         sprintf(acBody, "Please check \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
      exit(-1);
   }

   //if (bSitusOnly && bUseS01)
   //{
   //   LogMsgD("***** You cannot use -A and -U on the same command line\n");

   //   exit(-2);
   //}

   if (bReformatApn)
   {
      if (!memcmp(myCounty.acCntyCode, "MNO", 3))
         iRet = ApnXlat(myCounty.acCntyCode);
      else
         LogMsgD("*** WARNING: %.3s has not been coded.  Please contact Sony Nguyen", myCounty.acCntyCode);
      return iRet;
   }

   LogMsg(acVersion);
   if (bReformatMapLink)
      LogMsg0("Reformat MapLink enable");
   if (bSitusOnly)
      LogMsg0("Update Situs only enable");
   if (bIgnoreSitus)
      LogMsg0("Ignore Situs enable");
   if (bFormatDocLink)
      LogMsg0("Format DocLink enable");
   
   // Verify input file
   lRet = (long)getFileSize(acAdrFile);
   if (lRet < 1000)
   {
      if (bSendMail)
      {
         sprintf(acSubj, "***** %s - Error in MergeAdr.", myCounty.acCntyCode);
         sprintf(acBody, "Please check \"%s\" for possible corruption.", acAdrFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
      return -3;
   }

   // Open Geo file
   LogMsg("Open address file %s", acAdrFile);
   fdGeo = fopen(acAdrFile, "rb");
   if (fdGeo == NULL)
   {
      LogMsgD("***** ERROR opening Geo file: %s\n", acAdrFile);
      return -4;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** ERROR opening input file", acRawFile);
      return -5;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** ERROR creating output file", acOutFile);
      return -6;
   }

   // BUT GrGr APN is only 9 bytes long while roll record is 12 bytes long
   iApnLen = myCounty.iApnLen;
   memset(acGeoSave, 0x20, GEO_REC_MAX);

   // Get first GeoRec
   memset(acGeoRec, ' ', iGeoLen);
   pTmp = ReadGeoRec((char *)&acGeoRec[0], iGeoLen, fdGeo);
   bEof = (pTmp ? false:true);

   nBytesRead = iRecLen;

   // Copy skip record 
   memset(acBuf, ' ', iRecLen+ADD_SIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      if (memcmp(acBuf, "99999999", 8))
         bRet = WriteFile(fhOut, acBuf, iRecLen+ADD_SIZE, &nBytesWritten, NULL);
      else if (bSitusOnly)
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   acGeoSave[0] = 0;
   iMatch=iFixCity=iNoMatch=iUseSameBP=iWrongFips=iBadCity=iBadSuffix=iMisMatch=iBadAddr=0;
   iChgStrNum=iChgStr=iChgSfx=iChgCity=iChgZip=iChgZip4=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = 0;
            fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
               break;
         } else
            break;
      }

      // Blank situs if requested
      if (bIgnoreSitus)
      {
         memset((char *)&acBuf[OFF_S_STRNUM], ' ', SIZ_S_ADDR);
         memset((char *)&acBuf[OFF_S_ADDR_D], ' ', SIZ_S_ADDRB_D);
         memset((char *)&acBuf[OFF_S_HSENO], ' ', SIZ_S_HSENO);
      }

Geo_Reload:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "028129169", 9) )
      //   iTmp = 0;
#endif

      // Update Geo data
      iTmp = memcmp(acBuf, acGeoRec, iApnLen);
      if (!iTmp)
      {
         iMatch++;

         // Correct Situs address
         if (bUSPSOnly && acGeoRec[GEO_OFF_N_MCODE] == 'T')
            iRet = MergeCityZip(acGeoRec, acBuf);
         else if (bOverwriteSitus)
            iRet = MergeOvrSitus(acGeoRec, acBuf);
         else
            iRet = MergeSitus(acGeoRec, acBuf);

         if (!bSitusOnly)
         {
            if (!iRet)
            {
               // Format new data fields
               MakeNewFields(acGeoRec, acTmp);
               // Append to record
               lstrcpy((char *)&acBuf[iRecLen], acTmp);
            } else
               memset((char *)&acBuf[iRecLen], ' ', ADD_SIZE);

            // Save previous
            memcpy(acGeoSave, acGeoRec, iGeoLen);
         }

         // Get next GeoRec
         pTmp = ReadGeoRec((char *)&acGeoRec[0], iGeoLen, fdGeo);
         if (!pTmp)
            bEof = true;
      } else if (iTmp > 0)
      {
         // Skip GeoRec
         if (!bRegion)
         {
            LogMsg0("*** Mismatch R01= %.*s GEO=%.*s", iApnLen, acBuf, iApnLen, acGeoRec);
            iMisMatch++;
         }

         // Save previous
         memcpy(acGeoSave, acGeoRec, iGeoLen);

         pTmp = ReadGeoRec((char *)&acGeoRec[0], iGeoLen, fdGeo);
         if (pTmp)
            goto Geo_Reload;
      } else if (!bSitusOnly && acBuf[OFF_S_CITY] == ' ')
      {
         // No match for this APN
         // However, if the book-page are matching, use it
         if (!memcmp(acBuf, acGeoRec, myCounty.iCmpLen))
         {
            if (bDebug)
               LogMsg0("*** Use next page %.*s - %.*s", iApnLen, acBuf, iApnLen, acGeoRec);
            MakeGuessFields(acGeoRec, acTmp);

            // Guess situs city and zip if roll file is blank
            // 05/12/2021 - Don't guess, use what county provided only
            // GuessCityZip(acGeoRec, acBuf);

            lstrcpy((char *)&acBuf[iRecLen], acTmp);
            iUseSameBP++;
         } else if (!memcmp(acBuf, acGeoSave, myCounty.iCmpLen))
         {
            if (bDebug)
               LogMsg0("*** Use prev page %.*s - %.*s", iApnLen, acBuf, iApnLen, acGeoSave);
            MakeGuessFields(acGeoSave, acTmp);

            // Guess situs city and zip if roll file is blank
            //GuessCityZip(acGeoSave, acBuf);

            lstrcpy((char *)&acBuf[iRecLen], acTmp);
            iUseSameBP++;
         } else
         {
            // Doesn't match any, remap city name
            RemapCity(acBuf);

            // Blank out new fields
            memset((char *)&acBuf[iRecLen], ' ', ADD_SIZE);
            LogMsg0("*** No match R01= %.*s GEO=%.*s", iApnLen, acBuf, iApnLen, acGeoRec);
            iNoMatch++;
         }
      } else
      {
         // Blank out new fields
         memset((char *)&acBuf[iRecLen], ' ', ADD_SIZE);
         LogMsg0("*** No match R01= %.*s GEO=%.*s", iApnLen, acBuf, iApnLen, acGeoRec);
         iNoMatch++;
      }

#ifdef _DEBUG
      //if (acBuf[9] != '0')
      //   iTmp = 0;
#endif
      // Merge formatted APN
      iTmp = formatApn(acBuf, acTmp, &myCounty);
      memcpy((char *)&acBuf[OFF_APN_D], acTmp, iTmp);

      if (bReformatMapLink)
      {
         // Merge Maplink
         iTmp = formatMapLink(acBuf, acTmp, &myCounty);
         memcpy((char *)&acBuf[OFF_MAPLINK], acTmp, iTmp);

         // Create index map link
         if (getIndexPage(acTmp, acTmp1, &myCounty))
            memcpy((char *)&acBuf[OFF_IMAPLINK], acTmp1, iTmp);
      }

      // Format DOCLINK
      if (bFormatDocLink)
      {
         if (myCounty.iCntyID == 53)         // TRI
            Tri_FmtDocLinks(acBuf);
         else if (myCounty.iCntyID == 55)    // TUO
            Tuo_FmtDocLinks(acBuf);
      }

      // Create M_ADDR_D field
      if (bCreateMAdr)
         formatMAdr(acBuf);

      if (bRemovePQZoning)
      {
         memset(&acBuf[OFF_ZONE_X1], ' ', SIZ_ZONE_X1);
         memset(&acBuf[OFF_ZONE_X2], ' ', SIZ_ZONE_X2);
         memset(&acBuf[OFF_ZONE_X3], ' ', SIZ_ZONE_X3);
      }

      if (bSitusOnly)
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      else
         bRet = WriteFile(fhOut, acBuf, iRecLen+ADD_SIZE, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      if (acBuf[0] < '0')
      {
         LogMsg("***** Bad record %d: %.40s", lCnt, acBuf);
         break;
      }

      if (bDebug)
         LogMsg("*** No match R01= %.*s GEO=%.*s", iApnLen, acBuf, iApnLen, acGeoRec);
      memset((char *)&acBuf[iRecLen], ' ', ADD_SIZE);

      // Merge formatted APN
      iTmp = formatApn(acBuf, acTmp, &myCounty);
      memcpy((char *)&acBuf[OFF_APN_D], acTmp, iTmp);

      if (bReformatMapLink || acBuf[OFF_IMAPLINK] == ' ')
      {
         // Merge Maplink
         iTmp = formatMapLink(acBuf, acTmp, &myCounty);
         memset((char *)&acTmp[iTmp], ' ', SIZ_MAPLINK-iTmp);
         memcpy((char *)&acBuf[OFF_MAPLINK], acTmp, SIZ_MAPLINK);

         // Create index map link
         if (getIndexPage(acTmp, acTmp1, &myCounty))
            memcpy((char *)&acBuf[OFF_IMAPLINK], acTmp1, iTmp);
      }

      // Remap city
      RemapCity(acBuf);

      // Copy STRNUM to HSENO - remove 06/25/2018 spn
      //copyStrNum(acBuf);

      if (bSitusOnly)
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      else
         bRet = WriteFile(fhOut, acBuf, iRecLen+ADD_SIZE, &nBytesWritten, NULL);
      iNoMatch++;
      lCnt++;
   }

   LogMsg("Total output records        : %u", lCnt);
   LogMsg("Total Street number changed : %u", iChgStrNum);
   LogMsg("Total Street addr changed   : %u", iChgStr);
   LogMsg("Total Suffix only changed   : %u", iChgSfx);
   LogMsg("Total City & Zip changed    : %u", iChgCity);
   LogMsg("Total City & Zip guessed    : %u", iFixCity);
   LogMsg("Total Zip only changed      : %u", iChgZip);
   LogMsg("Total Zip4 only changed     : %u\n", iChgZip4);

   LogMsg("Total APN matched records   : %u", iMatch);
   LogMsg("Total unmatched records     : %u", iNoMatch);
   LogMsg("Total mismatched records    : %u", iMisMatch);
   LogMsg("Total BP-matched records    : %u", iUseSameBP);
   LogMsg("Total bad-city records      : %u", iBadCity);
   LogMsg("Total bad-suffix records    : %u", iBadSuffix);
   LogMsg("Total bad-fips records      : %u", iWrongFips);
   LogMsg("Total bad-addr records      : %u", iBadAddr);

   if (fdGeo)
      fclose(fdGeo);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   if (bSitusOnly)
      LogMsg("Total mail records updated  : %u", iUpdMail);

   if (!iMatch && bSendMail)
   {
      sprintf(acSubj, "***** %s - Error in MergeAdr. No APN matched.", myCounty.acCntyCode);
      sprintf(acBody, "Please check \"%s\" for possible corruption", acAdrFile);
      mySendMail(acIniFile, acSubj, acBody, NULL);
   } else if (iMatch < iNoMatch && bWarning)
   {
      sprintf(acSubj, "*** %s - WARNING: MergeAdr return low matching rate.", myCounty.acCntyCode);
      sprintf(acBody, "Match=%d, Unmatch=%d\nPlease review log file \"%s\" for more info", iMatch, iNoMatch, acLogFile);
      mySendMail(acIniFile, acSubj, acBody, NULL);
   }

   // Close log 
   close_log();

   printf("\nTotal output records: %u", lCnt);
   return 0;
}

