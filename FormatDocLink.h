// Notes: This file has to be included before R01.h
#ifndef  _FMT_DOCLINK
#define  _PDF_DOCLINK   1
#define  _TIF_DOCLINK   2

int Tri_FmtDocLinks(char *pR01, char *pCnty="TRI", int iType=_PDF_DOCLINK);
int Tuo_FmtDocLinks(char *pR01, char *pCnty="TUO", int iType=_PDF_DOCLINK);

#endif